<?php

Route::namespace('Admin')->group(function(){
    Route::namespace('Auth')->group(function(){
        Route::get('/login', ['uses' => 'LoginController@showLoginForm', 'as' => 'login']);
        Route::post('/login', ['uses' => 'LoginController@login', 'as' => 'postLogin']);
        Route::post('/logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);
    });

    Route::group(['middleware' => 'auth'], function(){

        Route::get('/dashboard',['uses' => 'DashboardController@index', 'as' => 'dashboard']);

        Route::get('/users/sale-network',['uses' => 'D5\SaleNetworkController@index','as' => 'users.sale-network.index']);
        Route::get('/users/{id}/accounts',['uses' => 'D5\AccountController@index','as' => 'users.accounts.index']);
        Route::get('/users/{id}/logs',['uses' => 'D5\LogController@index','as' => 'users.logs.index']);
        Route::get('get-user-account-log-info/{id}',['uses' => 'D5\LogController@ajaxGetLogInformation', 'as' => 'user.account.getLogInfo']);

        Route::get('/orders/temp',['uses' => 'D5\OrderTempController@index', 'as' => 'orders.temp.index']);
        Route::get('/orders/temp/download',['uses' => 'D5\OrderTempController@downloadExcel', 'as' => 'orders.temp.download']);
        Route::get('/orders/report/selection-form',['uses' => 'D5\OrderController@showSelectionForm', 'as' => 'orders.report.selection-form']);
        Route::get('/orders/report',['uses' => 'D5\OrderController@index', 'as' => 'orders.report.index']);
        Route::get('/orders/report/download',['uses' => 'D5\OrderController@downloadExcel', 'as' => 'orders.report.download']);

        Route::get('/language/set-locale/{locale}',['uses' => 'D5\LanguageController@setLocale', 'as' => 'language.set-locale']);

        Route::get('/ball/users/sale-network',['uses' => 'BALL\SaleNetworkController@show','as' => 'ball.sale-network.index']);
        Route::get('/ball/users/{id}/accounts',['uses' => 'BALL\AccountController@show','as' => 'ball.account.index']);
        Route::get('/ball/users/{id}/logs',['uses' => 'BALL\LogController@show','as'=> 'ball.log.index']);
        Route::get('/ball/get-user-account-log-info/{id}',['uses' => 'BALL\LogController@ajaxGetLogInformation', 'as' => 'ball.account.getLogInfo']);
        Route::get('/ball/orders/temp',['uses' => 'BALL\OrderTempController@show', 'as' => 'ball.orders.temp.index']);
        Route::get('/ball/orders/temp/download',['uses' => 'BALL\OrderTempController@downloadExcel', 'as' => 'ball.orders.temp.download']);
        Route::get('/ball/orders/{ticket}/get-order-detail',['uses' => 'BALL\OrderController@ajaxGetOrderDetail', 'as' => 'ball.orders.report.get-order-detail']);
        Route::get('/ball/report/selection-form',['uses' => 'BALL\OrderController@showSelectionForm','as' => 'ball.orders.report.selection-form']);
        Route::get('/ball/report',['uses' => 'BALL\OrderController@show','as' => 'ball.orders.report.index']);
        Route::get('/orders/report/download',['uses' => 'BALL\OrderController@downloadExcel', 'as' => 'orders.report.download']);
        Route::get('/ball/orders/temp/{ticket}/get-order-detail',['uses' => 'BALL\OrderTempController@ajaxGetOrderDetailTemp', 'as' => 'ball.orders.temp.get-order-detail']);


        Route::get('/5d/betting', ['uses' => 'D5\BettingController@index', 'as' => '5d.betting']);
        Route::get('/5d/record', ['uses' => 'D5\BettingController@getRecord', 'as' => '5d.record']);
        Route::get('/5d/report', ['uses' => 'D5\ReportController@showReportForm', 'as' => '5d.report']);
        Route::get('/5d/report/download', ['uses' => 'D5\ReportController@downloadReport', 'as' => '5d.report.download']);
        Route::get('/5d-report/download/today',['uses' => 'D5\ReportController@downloadReportToday', 'as' => 'download-today-report']);

        Route::get('/5d/record/{cycle}', ['uses' => 'D5\BettingController@getRecordByCycle', 'as' => '5d.record.cycle']);
        Route::get('/5d/record/{cycle}/download', ['uses' => 'D5\BettingController@downloadRecordCycle', 'as' => '5d.record.cycle.download']);
        Route::post('/5d/order','D5\BettingController@order');
        Route::get('/ajax-get-ticket-by-cycle', ['uses' => 'D5\BettingController@ticket_info']);
    });


});


Route::namespace('Client')->group(function(){
    Route::get('/',function(){ return redirect()->route('login');
    });
});
