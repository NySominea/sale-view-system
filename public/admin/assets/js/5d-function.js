function formatNumber(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function updateTotal(){
    var total = 0
    for(i =0; i<bet_body.length; i++){
        total += (bet_body[i].quantity * bet_body[i].single_amount)
    }

    $('#btn-total').val(`${total} (R)`)
}

validateInput = () => {
    var first_order = $('#first-order').val()
    var second_order = $('#second-order').val()
    var amount_order = $('#amount-order').val()


    if(!first_order){
        show_error('Input First Number!') 
        return false
    } 
    else if(!amount_order){
        show_error('Input Amount!')
        return false
    }
    else if(!JSON.parse(postDataSelector.val()).length){
        show_error('Please choose a prize!')
        return false
    }else if(amount_order <= 0 || amount_order % 100 != 0){
        show_error('Amount must be greater than zero and 100 fold!')
        return false
    }

    return true
}

function resetInput(){
    $('#first-order').val('')
    $('#second-order').val('')
    $('#amount-order').val('')
    $('.btn-all-post').removeClass("btn-active")
    $('.post-b').removeClass("btn-active")
    $('.post-c').removeClass("btn-active")
    $('.post-d').removeClass("btn-active")

    prizes = ['A']

    $('#first-order').focus()
}

function generateId() {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < 10; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function show_invalid_input(){
    show_error('Illegal Input')
}

function show_error(message){
    toastr.clear()
    toastr.error(message, 'Invalid Input')
}

function permute(string) {
    if (string.length < 2) return string; // This is our break condition
  
    var permutations = []; // This array will hold our permutations
    for (var i = 0; i < string.length; i++) {
      var char = string[i];
  
      // Cause we don't want any duplicates:
      if (string.indexOf(char) != i) // if char was used already
        continue; // skip it this time
  
      var remainingString = string.slice(0, i) + string.slice(i + 1, string.length); //Note: you can concat Strings via '+' in JS
  
      for (var subPermutation of permute(remainingString))
        permutations.push(char + subPermutation)
    }
    return permutations;
}

var combinations = function (string)
{
    var result = [];

    var loop = function (start,depth,prefix)
    {
        for(var i=start; i<string.length; i++)
        {
            var next = prefix+string[i];
            if (depth > 0)
                loop(i+1,depth-1,next);
            else
                result.push(next);
        }
    }

    for(var i=0; i<string.length; i++)
    {
        loop(0,i,'');
    }

    return result;
}

function getOnly3DigitsElement(ele) {
    return ele.length == 3;
}

function submit(){

    if(bet_body.length <= 0) show_error('Place at least one order')
    else{
        $.ajax({
            type: 'post',
            url: '/5d/order',
            dataType: 'json',
            data: {
                bet_body: bet_body_column
            },
            success: function(res){

                if(res.code == 403){
                    show_error(res.message)
                    return;
                }

                toastr.clear()
                toastr.success('Submitted!', 'Success')

                $('#show-balance').html(`Your Balance: ${res.data.balance} (R)`)
                bet_body = []
                bet_body_column = []

                updateTotal()
 
                $('#table-panel').html(`
                    <div class="table-bet">
                        <div class="table-responsive">
                            <table class="table table-borderless active" id="table-0" data-index="0">
                                <thead>
                                    <tr><th class="text-center pb-0"><span class="h6 mb-0">Column 0</span></th></tr>
                                    <tr>
                                        <th id="header_0">
                                            <span>2D:</span> <span>0</span><br>
                                            <span>3D:</span> <span>0</span><br>
                                            <span>5D:</span> <span>0</span><br>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody id="column_0"></tbody>
                            </table>
                        </div>
                    </div>
                `)

                $('#table-0').addClass('active')
                active_column = 0
            }
        })
    }

    
}