    var bet_body = []
    var bet_body_column = []
    var active_column = 0
    var first_order_allowed_characters = [
        '1','2','3','4','5','6','7','8','9','0','x','X','s','S','/','*'
    ]

    var second_order_allowed_characters = [
        '1','2','3','4','5','6','7','8','9','0'
    ]

    var utility_characters_code = [
        8,9
    ];

    var special_case = null

    var allowed_basic_digit = [2,3,5]

    $('#first-order').keypress(function(e) {
        console.log(e.keyCode)

        if(e.keyCode === 43){
            $('#second-order').focus()
            e.preventDefault()
            return
        }

        var character = String.fromCharCode(e.keyCode).toLowerCase()
        
        if(!first_order_allowed_characters.includes(character) && (!utility_characters_code.includes(e.keyCode))){
            e.preventDefault()
            return
        }else{
            if(character == '/'){
                character = 'x'
                special_case = '/'
            }else if(character == '*'){
                character = 's'
                special_case = '*'
            }
        }

        var value = $(this).val().toLowerCase()

        if(character == 'x'){
            //check if x already exist in value
            //check if s presented 
            //check if x3 already presented

            if(
                !value ||
                value.includes(character) || 
                value.includes('s') ||
                value.includes('x3') 
                
            ) e.preventDefault()
        }else if(character == 's'){
            //check if s presented more than 2 times
            var count = (value.match(/s/g) || []).length;
            if(count >= 2) e.preventDefault() 

            //check if x already presented
            //check if x3 already presented
            if(value.includes('x') || value.includes('x3')) e.preventDefault()
        }else if(value.includes('x3')){
            if(e.keyCode != 8 && e.keyCode != 9) e.preventDefault();
        }else if(e.keyCode == 13){
            $('#btn-order').click()
        }
    })

    $('#first-order').keyup(function(e){
        if(special_case && special_case === '/'){
            $('#first-order').val(
                $(this).val().replace('/','s')
            )
        }else if(special_case && special_case === '*'){
            $('#first-order').val(
                $(this).val().replace('*','x')
            )
        }

        special_case = null
    })

    $('#second-order').keypress(function(e) {

        if(e.keyCode === 43){
            $('#amount-order').focus()
            e.preventDefault()
            return
        }

        var character = String.fromCharCode(e.keyCode)
        if(!second_order_allowed_characters.includes(character) && (!utility_characters_code.includes(e.keyCode)))
            e.preventDefault()
    })

    $('#btn-order').click(() => {
        if(validateInput()){
            var first_order = $('#first-order').val().toLowerCase()
            var second_order = $('#second-order').val().toLowerCase()
            var amount_order = $('#amount-order').val()

            var quantity = ''
            var order_body = {}

            prizes = JSON.parse(postDataSelector.val())
            

            //check if x type
            if(first_order.includes('x3')){
                if(first_order.replace('x3','').length != 5){
                    show_invalid_input()
                    return
                }

                var numbers = combinations(first_order.replace("x3",""))
                numbers = numbers.filter(getOnly3DigitsElement)
                var arr = []

                for(i=0; i< numbers.length; i++){
                    permutations = permute(numbers[i])
                    arr = arr.concat(permutations)
                }
                
                arr = Array.from(new Set(arr))
                quantity = arr.length * prizes.length

                order_body = {
                    "count": {
                        "2D": 0,
                        "3D": quantity * amount_order,
                        "5D": 0
                    },
                    "identifier": generateId(),
                    "prize" : prizes,
                    "play_type" : "X3",
                    "bet_type": first_order.replace("x3","").length+'D',
                    "format_number": first_order,
                    "number" : first_order.replace('x3',''),
                    "single_amount" : amount_order,
                    "quantity": quantity
                }
                
            }else if(first_order.includes('x')){
                first_order = first_order.substr(0, first_order.length - 1)

                //check if length of number in range of 2,3,5
                if(!allowed_basic_digit.includes(first_order.length)){
                    show_invalid_input()
                    return
                }

                permutations = permute(first_order)
                quantity = permutations.length * prizes.length

                var count = {}
                count['2D'] = first_order.length == 2 ? quantity * amount_order : 0
                count['3D'] = first_order.length == 3 ? quantity * amount_order : 0
                count['5D'] = first_order.length == 5 ? quantity * amount_order : 0

                order_body = {
                    "count": count,
                    "identifier": generateId(),
                    "prize" : prizes,
                    "play_type" : "X",
                    "bet_type": first_order.length+'D',
                    "format_number": first_order+'x',
                    "number" : first_order,
                    "single_amount" : amount_order,
                    "quantity": quantity
                }
            }else if(first_order.includes('s')){
                if(!allowed_basic_digit.includes(first_order.length)) {
                    show_invalid_input()
                    return
                }
                var count = (first_order.match(/s/g) || []).length;
                if(count == 1) quantity = 10 * prizes.length
                else if(count == 2) quantity =  100 * prizes.length

                count = {}
                count['2D'] = first_order.length == 2 ? quantity * amount_order : 0
                count['3D'] = first_order.length == 3 ? quantity * amount_order : 0
                count['5D'] = first_order.length == 5 ? quantity * amount_order : 0

                order_body = {
                    "count": count,
                    "identifier": generateId(),
                    "prize" : prizes,
                    "play_type" : "S",
                    "bet_type": first_order.length+'D',
                    "format_number": first_order,
                    "number" : first_order,
                    "single_amount" : amount_order,
                    "quantity": quantity
                }
            }else{
                if(second_order){
                    if(!allowed_basic_digit.includes(first_order.length) || first_order.length != second_order.length || second_order <= first_order){
                        show_invalid_input()
                        return
                    }

                    quantity = (second_order - first_order + 1) * prizes.length

                    var count = {}
                    count['2D'] = first_order.length == 2 ? (quantity * amount_order) : 0
                    count['3D'] = first_order.length == 3 ? (quantity * amount_order) : 0
                    count['5D'] = first_order.length == 5 ? (quantity * amount_order) : 0

                    order_body = {
                        "count": count,
                        "identifier": generateId(),
                        "prize" : prizes,
                        "play_type" : "RANGE",
                        "bet_type": first_order.length+'D',
                        "format_number": `${ first_order }-${ second_order }`,
                        "number" : `${ first_order }-${ second_order }`,
                        "single_amount" : amount_order,
                        "quantity": quantity
                    
                    }
                }else{
                    if(!allowed_basic_digit.includes(first_order.length)){
                        show_invalid_input()
                        return
                    }

                    quantity = prizes.length

                    var count = {}
                    count['2D'] = first_order.length == 2 ? quantity * amount_order : 0
                    count['3D'] = first_order.length == 3 ? quantity * amount_order : 0
                    count['5D'] = first_order.length == 5 ? quantity * amount_order : 0

                    order_body = {
                        "count": count,
                        "identifier": generateId(),
                        "prize" : prizes,
                        "play_type" : "NORMAL",
                        "bet_type": first_order.length+'D',
                        "format_number": `${ first_order }`,
                        "number" : `${ first_order }`,
                        "single_amount" : amount_order,
                        "quantity": quantity
                    
                    }
                }
            }

            bet_body.push(order_body)

            if(!bet_body_column[active_column]) bet_body_column[active_column] = []
            bet_body_column[active_column].push(order_body)

            var posts = JSON.parse(postDataSelector.val()).sort().join(',')

           
            $( `<tr><td>
                <p><strong> ${ order_body.format_number }=${ order_body.single_amount * order_body.quantity } (${ posts })</strong>
                    <span>
                        <a class="text-danger" href="javascript:void(0)" 
                        id="${ order_body.identifier }"
                        onclick="removeOrder(${ active_column },'${ order_body.identifier }')" data-identify="${ order_body.identifier }"> Remove </a>
                    </span>
                </p>
            </td></tr>`).appendTo(`#column_${active_column}`)

            var count2d = 0
            var count3d = 0
            var count5d = 0

            for(var i=0; i<bet_body_column[active_column].length; i++){
                count2d += bet_body_column[active_column][i]['count']['2D']
                count3d += bet_body_column[active_column][i]['count']['3D']
                count5d += bet_body_column[active_column][i]['count']['5D']
            }

            $(`#header_${ active_column }`).html(`
            <span>2D:</span> <span> ${ count2d }</span><br>
            <span>3D:</span> <span> ${ count3d }</span><br>
            <span>5D:</span> <span> ${ count5d }</span><br>`)            

            updateTotal()
            resetInput()
        }
    });

    function removeOrder(column,identifier){
        bet_body.splice(bet_body.findIndex(x => x.identifier === identifier),1);
        bet_body_column[column].splice(
            bet_body_column[column].findIndex(x => x.identifier === identifier),
            1
        )
        $('#'+identifier).closest("tr").remove();

        var count2d = 0
        var count3d = 0
        var count5d = 0

        for(var i=0; i<bet_body_column[column].length; i++){
            count2d += bet_body_column[column][i]['count']['2D']
            count3d += bet_body_column[column][i]['count']['3D']
            count5d += bet_body_column[column][i]['count']['5D']
        }

        $(`#header_${ column }`).html(`
        <span>2D:</span> <span> ${ count2d }</span><br>
        <span>3D:</span> <span> ${ count3d }</span><br>
        <span>5D:</span> <span> ${ count5d }</span><br>`)

        updateTotal()
    }

    $("#btn-submit").click(function(){
        // console.log(bet_body)
        // console.log(bet_body_column)
        submit()
    })

    $('#amount-order').keypress(function(e) {
        var character = String.fromCharCode(e.keyCode).toLowerCase()
        if(!second_order_allowed_characters.includes(character) && (!utility_characters_code.includes(e.keyCode)))
            e.preventDefault()
    })

    $("#order-table").on("click", "#btn-remove", function() {
        var identifier = $(this).attr('data-identifier')

        bet_body.splice(bet_body.findIndex(x => x.identifier === identifier),1)
        updateTotal()

        $(this).closest("tr").remove();
    });

    $('#amount-order').keypress((e) => {
        if(e.keyCode === 43){
            $('#first-order').focus()
        }else if(e.keyCode === 13){
            $('#btn-order').click()
        }
    })


    