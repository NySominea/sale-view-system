
    const firstInputNo = $('#first-order')
    const secondInputNo = $('#second-order')
    const amountInputNo = $('#amount-order')
    const clear = $('#clear')

    var prizes = ['A']
    $('.post-a').addClass('btn-active')

    function check(){
        if($('.post-a').hasClass("btn-active") && $('.post-b').hasClass("btn-active")
        && $('.post-c').hasClass("btn-active") && $('.post-d').hasClass("btn-active")){
            $('.btn-all-post').addClass("btn-active")
        }else{
            $('.btn-all-post').removeClass("btn-active")
        }
    }

    $('.btn-post').click(function() {
        
        var text = $(this).text()
        if(prizes.includes(text)){
            prizes.splice(
                prizes.indexOf(text),1
            )
        }else{
            prizes.push(text)
        }

        $(this).toggleClass("btn-active");
        check();
      });

    $('.btn-all-post').click(function() {
       // $(this).toggleClass("btn-active");

        if($('.btn-post').hasClass('btn-active') && !$('.btn-all-post').hasClass('btn-active')){
            $('.btn-post').addClass('btn-active');
            $(this).addClass("btn-active")
            prizes = ['A','B','C','D']
        }else if($('.btn-post').hasClass('btn-active') && $('.btn-all-post').hasClass('btn-active')){
            $('.btn-post').removeClass('btn-active');
            $(this).removeClass("btn-active")
            prizes = []
        }else{
            $('.btn-post').addClass('btn-active');
            $(this).addClass("btn-active")
            prizes = ['A','B','C','D']
        }

    }); 


    /**
     * PAGE RECORD
     */
    const cycle = $('.table-cycle .cycle')
    const tableTicket = $('.table-ticket')

    // console.log(Date.parse(new Date(1562909400)).toString('yyyy-MM-dd H:i:s'))
    cycle.click(function () {
        cycleSelected($(this))
    })

    $(document).on("click", '.table-ticket .ticket', function () {
        ticketSelected($(this))
    });


    function cycleSelected(_this){
        cycle.removeClass('active')
        _this.addClass('active')
        const id = _this.data('id')
        $.ajax({
            type: 'GET',
            url: '/ajax-get-ticket-by-cycle',
            data: { cycle_id: id },
            success: function (response) {
                let data = '';
                const orders = response.orders
                const total = response.total

                if(orders.length > 0){
                    orders.forEach(function(order){
                        data += `<tr class='ticket' data-ticket='${JSON.stringify(order)}' style='cursor:pointer'>
                                    <td>${order.created_at}</td>
                                    <td>${order.amount}</td>
                                    <td>${order.ticket}</td>
                                </tr>`;
                    })
                }else{
                    data = `<tr>
                                <td colspan='3' class='text-center'>No data</td>
                            </tr>`
                }
                console.log(total)

                tableTicket.find('tbody').html(data)
                tableTicket.find('tfoot').html(`
                    <tr>
                        <td class="text-center bg-warning" colspan="3" style="font-style: blod; font-size: 14px;">
                            <span class="m-0">Amount: <span id="cycle-amount">${ total.total_amount } (R)</span>, </span>
                            <span class="m-0">Total Win: <span id="cycle-total-win">${ total.total_win_amount } (R)</span>, </span>
                            <span class="m-0">Win 2D: <span id="cycle-win-2d">${ total.total_2d_amount } (R)</span>, </span>
                            <span class="m-0">Win 3D: <span id="cycle-win-3d">${ total.total_3d_amount } (R)</span>, </span>
                            <span class="m-0">Win 5D: <span id="cycle-win-5d">${ total.total_5d_amount } (R)</span>, </span>
                        </td>
                    </tr>
                `)
            },
            error: function () {

            }
        })
    }

    function ticketSelected(_this){
        const ticketDetail = _this.data('ticket')
        const ticket = $('#ticket')
        const amount = $('#amount')
        const winAmount = $('#win-amount')
        const createdAt = $('#created-at')
        const resultTime = $('#result-time')
        const tableTicketDetail = $('.table-ticket-detail')

        ticket.html(ticketDetail.ticket)
        amount.html(ticketDetail.amount + ' R')
        winAmount.html(ticketDetail.win_amount + ' R')
        createdAt.html(ticketDetail.created_at)
        resultTime.html(moment(new Date(ticketDetail.result_time * 1000)).format('YYYY-MM-DD HH:mm A'))

        let data = '';
        const betContents = JSON.parse(ticketDetail.bet_content)
        betContents.forEach(function (bet) {
            data += `<tr>
                        <td>${bet[0]}</td>
                        <td>${bet[1]}</td>
                        <td>${bet[3]}</td>
                        <td>${bet[2] + ' R'}</td>
                        <td>${parseInt(bet[5]).toFixed(0) + ' R'}</td>
                        <td>${ !ticketDetail.is_settle ? 'Waiting' : parseInt(bet[5]).toFixed(0) == 0 ? 'Try again' : 'Win'}</td>
                    </tr>`;
        })
        tableTicketDetail.find('tbody').html(data)
    }
