@if(session('error')) 
<div class="alert bg-danger text-white alert-styled-left alert-dismissible">
    <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
    <span class="font-weight-semibold">Oh snap!</span> {{session('error')}}
</div>
@endif