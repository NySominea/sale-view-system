<div class="m-dropzone dropzone m-dropzone--primary dz-clickable" action="{{ route('settings.saveImage') }}" data-key="{{$key}}" id="dropzone_{{$key}}">
    @csrf
    <div class="m-dropzone__msg dz-message needsclick">
        <h3 class="m-dropzone__msg-title">Drop image here or click to upload.</h3>
        <span class="m-dropzone__msg-desc">Single Image Upload</span>
    </div>
</div>
@php $image = isset($setting[$key]) ? $setting[$key]->getMedia('images')->first() : null; @endphp
@if($image)
    <input type="hidden" name="{{$key}}" id="{{$key}}" value="" data-model-id="{{$setting[$key]->id}}" data-name="{{$image->file_name}}" data-size="{{$image->size}}" data-url="{{ asset($image->getUrl()) }}">
@else
    <input type="hidden" name="{{$key}}" id="{{$key}}">
@endif