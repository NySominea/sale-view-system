<div class="navbar navbar-expand-md navbar-dark">
    <div class="navbar-brand">
        <a href="index.html" class="d-inline-block">
            <img src="../../../admin/global_assets/images/logo_light.png" alt="">
        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item dropdown" data-popup="tooltip" title="" data-placement="bottom" id="bottom" data-original-title="Switch Product">
                <a href="#" class="navbar-nav-link dropdown-toggle caret-0">
                    <i class="icon-git-compare"></i>
                    <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0">2</span>
                </a>
            </li>
        </ul>

        <span class="badge bg-success ml-md-3 mr-md-auto">Online</span>

        <ul class="navbar-nav">
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    @if(app()->getLocale() == 'en')
                        <img src="{{asset('admin/assets/images/en.png')}}" class="mr-2" height="15" alt="">
                        <span>English</span>
                    @elseif(app()->getLocale() == 'kh')
                        <img src="{{asset('admin/assets/images/kh.png')}}" class="mr-2" height="15" alt="">
                        <span>Khmer</span>
                    @elseif(app()->getLocale() == 'zh')
                        <img src="{{asset('admin/assets/images/zh.png')}}" class="mr-2" height="15" alt="">
                        <span>Chinese</span>
                    @endif
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="{{route('language.set-locale','en')}}" class="dropdown-item"><img width="20px" src="{{asset('admin/assets/images/en.png')}}"> English</a>
                    <div class="dropdown-divider"></div>
                    <a href="{{route('language.set-locale','kh')}}" class="dropdown-item"><img width="20px" src="{{asset('admin/assets/images/kh.png')}}"> Khmer</a>
                    <div class="dropdown-divider"></div>
                    <a href="{{route('language.set-locale','zh')}}" class="dropdown-item"><img width="20px" src="{{asset('admin/assets/images/zh.png')}}"> Chinese</a>
                </div>
            </li>
            <li class="nav-item dropdown dropdown-user">
                @php $auth = session()->get('auth'); @endphp
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    <img src="../../../admin/global_assets/images/demo/users/face11.jpg" class="rounded-circle mr-2" height="34" alt="">
                    <span>{{ $auth ? $auth->UserName : 'Sethei Lottery' }}</span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="#" class="dropdown-item"><i class="icon-key"></i> Edit Password</a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();"><i class="icon-switch2"></i> Logout</a>
                    <form id="frm-logout" action="{{route('logout')}}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </li>
        </ul>
    </div>
</div>