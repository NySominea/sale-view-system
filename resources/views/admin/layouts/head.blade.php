<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Sethei Lottery Company</title>

<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
{{-- <link href="/global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
<link href="/assets/css/layout.min.css" rel="stylesheet" type="text/css">
<link href="/assets/css/components.min.css" rel="stylesheet" type="text/css">
<link href="/assets/css/colors.min.css" rel="stylesheet" type="text/css"> --}}
<!-- /global stylesheets -->
<link href="{{ asset('admin/assets/css/all.min.css')}}" rel="stylesheet" type="text/css">
<link href="/admin//assets/css/custom.css" rel="stylesheet" type="text/css">
<link href="{{asset('admin/assets/css/toastr.min.css')}}" rel="stylesheet" type="text/css" />

@yield('custom-css')