<script src="{{asset('admin/global_assets/js/main/jquery.min.js')}}"></script>
<script src="{{asset('admin/global_assets/js/main/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('admin/global_assets/js/plugins/loaders/blockui.min.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="{{asset('admin/assets/js/app.js')}}"></script>
<script src="{{asset('admin/assets/js/toastr.min.js')}}"></script>

@yield('page-script')

<script src="{{asset('admin/assets/js/custom.js')}}"></script>
