<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Sethei 5D</div> <i class="icon-menu" title="Main"></i></li>

@php

    $auth = session()->get('auth');
    $lv = $auth->level;
    $parent_id = $auth->parent_id;
    $id = $auth->id;
    // dd(in_array(Route::currentRouteName(), ['users.sale-network.index','users.accounts.index','users.logs.index']));
@endphp

<li class="nav-item">
    <a href="{{route('users.sale-network.index')}}?level={{$lv-1}}&parent_id={{$parent_id}}&id={{$id}}" class="nav-link {{ in_array(Route::currentRouteName(), ['users.sale-network.index','users.accounts.index','users.logs.index']) ? ' active' : ''}}"><i class="icon-home4"></i> <span>Sale Network</span></a>
</li>
<li class="nav-item">
    <a href="{{route('orders.temp.index')}}?level={{$lv}}&parent_id={{$id}}" class="nav-link "><i class="icon-home4"></i> <span>Order</span></a>
</li>
<li class="nav-item">
    <a href="{{route('orders.report.selection-form')}}" class="nav-link {{ in_array(Route::currentRouteName(), ['orders.report.selection-form','orders.report.index']) ? ' active' : ''}}"><i class="icon-home4"></i> <span>History List</span></a>
</li>
@if(in_array(auth()->user()->username,get_kong_users()))
<li class="nav-item nav-item-submenu">
    <a href="#" class="nav-link"><i class="icon-coin-dollar"></i> <span>Gaming</span></a>

    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
        <li class="nav-item"><a href="{{ route('5d.betting') }}" class="nav-link">Bet</a></li>
        <li class="nav-item"><a href="{{ route('5d.record') }}" class="nav-link">Record</a></li>
        <li class="nav-item"><a href="{{ route('5d.report') }}" class="nav-link">Report</a></li>
    </ul>
</li>
@endif
{{-- <li class="nav-item">
    <a href="{{route('user-manager.amount-log')d21bd0fcea542bd7d0aad72b75886d0a0a7deea9}}" class="nav-link"><i class="icon-home4"></i> <span>Account Log</span></a>
</li> --}}


<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Sethei Ball</div> <i class="icon-menu" title="title"></i></li>
<li class="nav-item">
    <a href="{{route('ball.sale-network.index')}}?level={{$lv-1}}&parent_id={{$parent_id}}&id={{$id}}" class="nav-link {{ in_array(Route::currentRouteName(), ['ball.sale-network.index','ball.account.index','ball.log.index']) ? ' active' : ''}}"><i class="icon-home4"></i> <span>Sale Network</span></a>
</li>
<li class="nav-item">
    <a href="{{route('ball.orders.temp.index')}}?level={{$lv}}&parent_id={{$id}}" class="nav-link"><i class="icon-home4"></i> <span>Order</span></a>
</li>
<li class="nav-item">
    <a href="{{route('ball.orders.report.selection-form')}}" class="nav-link {{ in_array(Route::currentRouteName(), ['ball.orders.report.selection-form','ball.orders.report.index']) ? ' active' : ''}}"><i class="icon-home4"></i> <span>History List</span></a>
</li>
