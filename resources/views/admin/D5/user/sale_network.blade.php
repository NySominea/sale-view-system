
@extends('admin.layouts.master')
@section('content')
<!-- Page header -->
@php 
    $auth = session()->get('auth');
    $lv = $auth->level -1; 
    $parent_id = $auth->parent_id;
    $id = $auth->id;
   
@endphp

<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Sale Network - 5D</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_USER_LIST'] }}</span>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_SALE_NET'] }}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">
    {{ Form::open(['route' => 'users.sale-network.index', 'method' => 'GET', 'class' => 'form-inline d-block d-sm-flex']) }}
        <div class="form-group mb-2 mr-2">
            {{Form::select("state",['all' => 'ALL','1' => 'OPEN','0' => 'CLOSE'], isset($_GET['state']) ? $_GET['state'] : null,["class" => "form-control form-control-sm", 'id' => 'state'])}}
        </div>
        {{-- <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                {{Form::text("keyword",request()->keyword,["class" => "form-control", "placeholder" => 'User Name / Account No','id' => 'keyword'])}}
                <span class="input-group-append">
                    {{Form::submit('Search',['class' => 'btn btn-primary appendQueryBtn'])}}
                </span>
            </div>
        </div>
        <a href="{{route('users.sale-network.index')}}" class="btn btn-warning btn-sm mb-2">Reset</a> --}}
    {{ Form::close() }}
    

    <div class="breadcrumb pt-2">
        <a href="{{route('users.sale-network.index')}}?level={{$lv}}&parent_id={{$parent_id}}&id={{$id}}" class="breadcrumb-item pt-0">{{ $settings['language']['LANG_MENU_SALE_NET'] }}</a>
        @if(isset($topLevelUsers))
            @foreach($topLevelUsers as $row)
                @if($row->level != request()->level && request()->level >= $lv && $row->level >= $lv)
                    @if($lv == $row->level)
                    <a href="{{route('users.sale-network.index').'?level='.$row->level.'&parent_id='.$row->id.'&id='.$id}}" class="breadcrumb-item pt-0">{{getUserLevelTitle($row->level)}} {{$row->username}}</a>
                    @else 
                    <a href="{{route('users.sale-network.index').'?level='.$row->level.'&parent_id='.$row->id}}" class="breadcrumb-item pt-0">{{getUserLevelTitle($row->level)}} {{$row->username}}</a>
                    @endif
                @else 
                    <span class="breadcrumb-item active pt-0">{{getUserLevelTitle($row->level)}} {{ $row->username }}</span>
                @endif
            @endforeach
        @endif
    </div>

    <div class="card">
        <div class="table-responsive">
            @if(!request()->keyword)
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>{{getUserLevelTitle(request()->level + 1)}}</th>
                        <th>{{ $settings['language']['LANG_LABEL_ACCOUNT_NO'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_BALANCE'] }}(R)</th>
                        <th>{{ $settings['language']['LANG_LABEL_FROZEN'] }}(R)</th>
                        <th>Earn Rebate(R)</th>
                        <th>{{ $settings['language']['LANG_LABEL_UNPAID_REBATE'] }}(R)</th>
                        <th>{{ $settings['language']['LANG_LABEL_WIN_AMOUNT'] }}(R)</th>
                        <th>{{ $settings['language']['LANG_LABEL_UNDERS'] }}</th>	 
                        <th>Action</th>			
                    </tr>
                </thead>
                <tbody>
                    @if(isset($users) && $users->count() > 0)
                        @foreach($users as $key => $row)
                            <tr>
                                @if($row->children->count() > 0)
                                    <td><a href="{{route('users.sale-network.index').'?level='.$row->level.'&parent_id='.$row->id}}">{{ $row->username }}</a></td>
                                    <td><a href="{{route('users.sale-network.index').'?level='.$row->level.'&parent_id='.$row->id}}">{{ $row->account_number }}</a></td>
                                @else 
                                    <td>{{ $row->username }}</td>
                                    <td>{{ $row->account_number }}</td>
                                @endif
                                <td>{{ currencyFormat($row->userCashAccount ? $row->userCashAccount->balance : 0) }}</td>
                                <td>{{ currencyFormat($row->userCashAccount ? $row->userCashAccount->frozen : 0) }}</td>
                                <td>{{ currencyFormat($row->commission_5d) }}</td>
                                <td>{{ currencyFormat($row->current_commission_5d) }}</td>
                                <td>{{ currencyFormat($row->win_money_5d) }}</td>
                                <td>{{ $row->children->count() }}</td>
                                <td class="group-btn-action">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-outline bg-teal-400 text-teal-400 border-teal-400 border-2 dropdown-toggle mr-1" data-toggle="dropdown" aria-expanded="false"><i class="icon-cog5"></i> Action</button>
                                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(135px, 36px, 0px);">
                                            {{-- <a href="{{route('users.sale-network.edit',$row->id)}}" class="dropdown-item"><i class="icon-pen6"></i> Setting</a> --}}
                                            <div class="dropdown-divider m-0"></div>
                                            <a href="{{route('users.accounts.index',$row->id)}}" class="dropdown-item"><i class="icon-credit-card"></i> Account</a>
                                            <div class="dropdown-divider m-0"></div>
                                            <a href="{{route('users.logs.index',$row->id)}}" class="dropdown-item"><i class="icon-list"></i> Account Log</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else 
                        <tr><td colspan="9">No Data</td></tr>
                    @endif
                </tbody>
            </table>
            @else 
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>{{ $settings['language']['LANG_LABEL_USER_TYPE'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_USERNAME'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_ACCOUNT_NO'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_BALANCE'] }}(R)</th>
                        <th>{{ $settings['language']['LANG_LABEL_FROZEN'] }}Frozen(R)</th>
                        <th>Earn Rebate(R)</th>
                        <th>{{ $settings['language']['LANG_LABEL_UNPAID_REBATE'] }}(R)</th>
                        <th>{{ $settings['language']['LANG_LABEL_WIN_AMOUNT'] }}(R)</th>
                        <th>Action</th>			
                    </tr>
                </thead>
                <tbody>
                    @if(isset($users) && $users->count() > 0)
                        @foreach($users as $key => $row)
                            <tr>
                                <td>{{ getUserLevelTitle($row->level) }}</td>
                                <td>{{ $row->username }}</td>
                                <td>{{ $row->account_number }}</td>
                                <td>{{ currencyFormat($row->userCashAccount ? $row->userCashAccount->balance : 0) }}</td>
                                <td>{{ currencyFormat($row->userCashAccount ? $row->userCashAccount->frozen : 0) }}</td>
                                <td>{{ currencyFormat($row->commission_5d) }}</td>
                                <td>{{ currencyFormat($row->current_commission_5d) }}</td>
                                <td>{{ currencyFormat($row->win_money_5d) }}</td>
                                <td class="group-btn-action">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-outline bg-teal-400 text-teal-400 border-teal-400 border-2 dropdown-toggle mr-1" data-toggle="dropdown" aria-expanded="false"><i class="icon-cog5"></i> Action</button>
                                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(135px, 36px, 0px);">
                                            <a href="{{route('users.sale-network.index')}}{{$row->level != 1 ? '?level='.($row->level-1).'&parent_id='.$row->parent_id: ''}}" class="dropdown-item"><i class="icon-users4"></i> Sale Network</a>
                                            <div class="dropdown-divider m-0"></div>
                                            {{-- <a href="{{route('users.sale-network.edit',$row->id)}}" class="dropdown-item"><i class="icon-pen6"></i> Setting</a>
                                            <div class="dropdown-divider m-0"></div> --}}
                                            {{-- <a href="{{route('users.accounts.show',$row->id)}}" class="dropdown-item"><i class="icon-credit-card"></i> Account</a> --}}
                                            <div class="dropdown-divider m-0"></div>
                                            <a href="{{route('users.logs.show',$row->id)}}" class="dropdown-item"><i class="icon-list"></i> Account Log</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else 
                        <tr><td colspan="9">No Data</td></tr>
                    @endif
                </tbody>
            </table>
            @endif
        </div>
        @if(isset($users) && count($users) > 0)
        <div class="card-footer">
            @if($users->hasMorePages())
                <div class="mb-2">
                    {!! $users->appends(Input::except('page'))->render() !!}
                </div>
            @endif
            <div>
                Showing {{$users->firstItem()}} to {{$users->lastItem()}}
                of  {{$users->total()}} entries
            </div>
        </div>
        @endif
    </div>
    
</div>
@endsection