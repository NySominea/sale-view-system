@extends('admin.layouts.master')

@section('custom-css')
    <style>
        .btn-post.btn-active {
            background: orangered;
            color:white
        }

        .btn-all-post.btn-active{
            background: orangered;
            color:white
        }

        /* .table.table-cycle tr.active,
        .table.table-cycle tr:hover{
            background-color: #ff7043;
            color: white;
        }

        .table tbody{
            overflow:auto;
            display: block;
            width:100%;
        }
        .table-cycle tbody{
            height:570px;
        }
        .table-ticket tbody{
            height:200px;
        }
        .table-ticket-detail tbody{
            height:150px;
        }
        .table-cycle thead, tbody tr,
        .table-ticket thead, tbody tr,
        .table-ticket-detail thead, tbody tr {
            display:table;
            width:100%;
            table-layout:fixed;
        } */

    </style>
@endsection

@section('content')

<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">System Betting - 5D</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">5D</span>
                <span class="breadcrumb-item active">Betting</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content p-xs-2">
    <div class="">
        {{-- <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Home</a>
                <a class="nav-item nav-link " id="nav-record-tab" data-toggle="tab" href="#nav-record" role="tab" aria-controls="nav-record" aria-selected="false">Record</a>
            </div>
        </nav> --}}

        {{-- <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab"> --}}
                <div class="row">
                    <div class="col-md-6 mb-2">
                        <p>Cycle Stop in time: </p>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <table class="table table-bordered" id="order-table">
                                    <thead>
                                        <th>Post</th>
                                        <th>Bet</th>
                                        <th>PC/U</th>
                                        <th>Amount</th>
                                        <th>Action</th>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>

                                <div class="btn-group btn-group-lg btn-block mt-2 mb-2" role="group" aria-label="First group">
                                    <button id="btn-submit" type="button" class="btn btn-primary mr-1">SUBMIT</button>
                                    <button id="btn-total" type="button" class="btn btn-warning ml-1">0 <span>(R)</span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-4 input-position text-center" >
                        <p class="text-center">NUMBER</p>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-lg-12">
                                <div class='num-order'>
                                    <input type="text" maxlength="7" class="form-control mb-3 text-center" id="first-order" placeholder="First order">
                                    <input type="text" maxlength="5" class="form-control mb-5 text-center" id="second-order" placeholder="Second order">
                                    <p>Amount</p>
                                    <input type="number" maxlength="100" class="form-control text-center" id="amount-order" placeholder="Bet amount"/>
                                </div>
                                <button type="button" class="btn btn-secondary text-center col-md-12 mt-1 mb-2" id="clear">Clear</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-8">
                        <div class='container group-num'>
                            <div class="row post-bet mb-1">
                                <div class="btn-group btn-group-lg mb-1 btn-block" role="group">
                                    <button type="button" class="btn mr-1 border-bottom post-a btn-post">A</button>
                                    <button type="button" class="btn mr-1 border-bottom post-b btn-post">B</button>
                                    <button type="button" class="btn mr-1 border-bottom post-c btn-post">C</button>
                                    <button type="button" class="btn mr-1 border-bottom post-d btn-post">D</button>
                                    <button type="button" class="btn border-bottom btn-all-post">4P</button>
                                </div>
                            </div>

                            <div class="row">
                                <div class="btn-group btn-group-lg mb-2 btn-block" role="group" aria-label="First group">
                                    <button type="button" class="btn btn-secondary btn-lg mr-2 col-md-3 btn-no">7</button>
                                    <button type="button" class="btn btn-secondary btn-lg mr-2 col-md-3 btn-no">8</button>
                                    <button type="button" class="btn btn-secondary btn-lg mr-2 col-md-3 btn-no">9</button>
                                    <button type="button" class="btn btn-secondary btn-lg col-md-3"><i class="icon-arrow-left8" style="font-size:13px"></i></button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="btn-group btn-group-lg btn-block mb-2" role="group" aria-label="First group">
                                    <button type="button" class="btn btn-secondary mr-2 col-md-3 btn-no">4</button>
                                    <button type="button" class="btn btn-secondary mr-2 col-md-3 btn-no">5</button>
                                    <button type="button" class="btn btn-secondary mr-2 col-md-3 btn-no">6</button>
                                    <button type="button" class="btn btn-secondary col-md-3 btn-s">S</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="btn-group btn-group-lg btn-block mb-2" role="group" aria-label="First group">
                                    <button type="button" class="btn btn-secondary mr-2 col-md-3 btn-no">1</button>
                                    <button type="button" class="btn btn-secondary mr-2 col-md-3 btn-no">2</button>
                                    <button type="button" class="btn btn-secondary mr-2 col-md-3 btn-no">3</button>
                                    <button type="button" class="btn btn-secondary col-md-3 btn-x">X</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="btn-group btn-group-lg btn-block mb-2" role="group" aria-label="First group">
                                    <button type="button" class="btn btn-secondary mr-2 col-md-3 btn-no">0</button>
                                    <button type="button" class="btn btn-secondary mr-2 col-md-3 btn-no" style="font-size:12px">00</button>
                                    <button type="button" class="btn btn-secondary mr-2 col-md-3 btn-x3" style="font-size:12px">X3</button>
                                    <button type="button" class="btn btn-secondary col-md-3"><i class="icon-arrow-down8" style="font-size:13px"></i></button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="btn-group btn-group-lg btn-block mb-2" role="group" aria-label="First group">
                                    <button id="btn-order" type="button" class="btn btn-primary mr-1 col-md-6">Order</button>
                                    <button type="button" class="btn btn-secondary ml-1 col-md-6">Input R</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {{-- </div> --}}

            {{-- <div class="tab-pane fade" id="nav-record" role="tabpanel" aria-labelledby="nav-record-tab">
                <div class="row">
                    <div class="col-md-4 mb-2">
                        <div class="row">
                            <div class="col-12">
                                <table class="table table-bordered table-cycle">
                                    <thead class="text-center">
                                        <th> Date and Time </th>
                                    </thead>
                                    <tbody>
                                        @foreach($recordCycles as $cycle)
                                        <tr data-id="{{$cycle['id']}}" class="cycle text-center " style="cursor:pointer">
                                            <td>{{ date('Y-m-d H:i A', $cycle['result_time']) }} </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="ticket-info">
                            
                            <div class="row">
                                <div class="col-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-ticket">
                                            <thead>
                                                <th>Date and Time</th>
                                                <th>Amount</th>
                                                <th>Ticket</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-center" colspan="3"> Please select a cycle</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td class="text-center bg-warning" colspan="3"><h6 class="m-0">Amount: <span id="cycle-amount">0</span></h6></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                           

                            
                            <div class="card mt-3" id="ticket-detail">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6">
                                            <p class="" style="margin-bottom: .0!important;"><label>Ticket: &nbsp;</label><span id="ticket"></span></p>
                                            <p style="margin-bottom: .0!important;"><label>Total: &nbsp;</label><span id="amount"></span></p>
                                            <p style="margin-bottom: .0!important;"><label>Total Win: &nbsp;</label><span id="win-amount"></span></p>
                                        </div>
                                        <div class="col-6">
                                            <p style="margin-bottom: .0!important;"><label>Date: &nbsp;</label><span id="created-at"></span></p>
                                            <p style="margin-bottom: .0!important;"><label>Result Time: &nbsp;</label><span id="result-time"></span></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="table-reponsive">
                                                <table class="table table-bordered table-ticket-detail">
                                                    <thead>
                                                        <th>Post</th>
                                                        <th>Number</th>
                                                        <th>PC/U</th>
                                                        <th>Amount</th>
                                                        <th>Win</th>
                                                        <th>Status</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="6" class="text-center">Please select a ticket</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           

                        </div>
                    </div>
                </div>
            </div> --}}
        {{-- </div> --}}

    </div>
</div>

@endsection

@section('custom-js')
    <script src="{{asset('admin/assets/js/5d-betting.js')}}"></script>
    <script src="{{asset('admin/assets/js/5d-function.js')}}"></script>
    <script src="{{asset('admin/assets/js/5d-betting-check-input.js')}}"></script>
@endsection
