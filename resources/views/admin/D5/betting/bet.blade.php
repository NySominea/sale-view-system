@extends('admin.layouts.master')

@section('custom-css')
    <style>

        .uniform-checker {
            color: white;
            border-color: white;
        }
        .uniform-checker span:after {
            font-size: 2rem
        }

        input[type=checkbox][data-fouc],
        .uniform-checker span,
        .uniform-checker{
            width: 2.25rem;
            height: 2.25rem;
        }

        .form-check .form-check-label {
            padding-left: 15px;
            font-size: 1.5rem;
            font-weight: bold;
        }

        table {
            table-layout: fixed;
            width: 100%;
        }
        table td {
            word-wrap: break-word;         /* All browsers since IE 5.5+ */
            overflow-wrap: break-word;     /* Renamed property in CSS3 draft spec */
        }
        .table thead th {
            border-bottom: none
        }
        .table.active{
            border: 2px solid #ff5722
        }
        .table.active thead {
            border-bottom: 2px solid #ff5722
        }
        .table{
            border: 2px solid gray
        }
        .table thead {
            border-bottom: 2px solid gray
        }
        .table-container{
            overflow: hidden;
            overflow-x: scroll;
        }
        .table-bet{
            min-width: 20%;
            max-width: 20%;
            margin-right: 5px;
        }
    </style>
@endsection

@section('content')

<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">System Betting - 5D</span></h4>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content p-xs-2" id="div-content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-2 bg-warning">
                    <div class="form-group mb-0" id="prizeArr">
                        @foreach(['A', 'B', 'C', 'D','4P'] as $index => $post)
                            <div class="form-check custom-control-inline post-{{$post}}">
                                <label class="form-check-label">
                                    <input type="checkbox" value="{{ $post }}" id="input-post-{{$post}}" class="form-check-input-styled-custom checkbox" {{ $post == 'A' ? 'checked' : '' }} data-fouc data-post="{{$post}}">
                                    {{ $post }}
                                </label>
                            </div>
                        @endforeach
                        <input type="hidden" name="posts" value='["A"]'>
                    </div>
                </div>
                <div class="col-12 pt-2 pb-0 mt-1" id="background-cycle" style="background: {{ isset($activeCycle) ? 'green' : 'red' }};">
                    <p class="pb-0" style="color:white" id="show-cycle">
                       
                    </p>
                    <p  style="color:white" id="show-balance">
                        Your Balance: {{ number_format($balance) }} (R)
                    </p>
                </div>
                {!! Form::hidden('cycle',$activeCycle,['id' => 'input-cycle']) !!}
                <div class="operate ml-2 mb-3">
                    <div class="row">
                        <div class="col-2 text-center pt-1">
                            <label class="text-center mb-0">Section 1</label>
                            <input class="form-control" id="first-order">
                        </div>
                        <div class="col-2 text-center pt-1">
                            <label class="text-center mb-0">Section 2</label>
                            <input class="form-control" id="second-order">
                        </div>
                        <div class="col-2 text-center pt-1">
                            <label class="text-center mb-0">Amount </label>
                            <input class="form-control" id="amount-order" type="number">
                        </div>
                        <div class="col-3 pt-3">
                            <button id="btn-order" class="btn btn-info mt-1 mr-1">Order</button>
                            <button class="btn btn-success mt-1 mr-1" id="btn-submit">Submit</button>
                            <button class="btn btn-warning ml-1 mt-1" id="previous-btn">Previous</button>
                            <button class="btn btn-warning ml-1 mt-1" id="next-btn">Next</button>
                            <button class="btn btn-danger mt-1" id="add-column-btn">Add Column</button>
                        </div>
                        <div class="col-2  pt-1 text-center">
                            <label class="text-center mb-0">Total Amount</label>
                            <input id="btn-total" class="btn btn-info" placeholder="0" readonly style="color:white">
                        </div>
                    </div>
                </div>
                <div class="data-list m-2">
                    <div class="row">
                        <div class="col-12">
                            <div class="table-container d-flex flex-row flex-nowrap" id="table-panel">
                                @for($i=0; $i<1; $i++)
                                <div class="table-bet">
                                    <div class="table-responsive">
                                        <table class="table table-borderless {{ $i == 0 ? 'active' : '' }}" id="table-{{$i}}" data-index="{{$i}}">
                                            <thead>
                                                <tr><th class="text-center pb-0"><span class="h6 mb-0">Column {{$i+1}}</span></th></tr>
                                                <tr>
                                                    <th id="header_{{ $i }}">
                                                        <span>2D:</span> <span>0</span><br>
                                                        <span>3D:</span> <span>0</span><br>
                                                        <span>5D:</span> <span>0</span><br>
                                                    </th>
                                                </tr>
                                            </thead>

                                            <tbody id="column_{{ $i }}"></tbody>
                                        </table>
                                    </div>
                                </div>
                                @endfor
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('page-script')
    <script src="/admin/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script src="/admin/global_assets/js/demo_pages/form_checkboxes_radios.js"></script>
@endsection

@section('custom-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/keyboardjs/2.5.0/keyboard.min.js" integrity="sha256-Fr92zk7ao90taVNuDw2o+bY7ST8jnfRx/hIQifc/Nps=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
    <script>
        
        const postDataSelector = $('input[name="posts"]')
        $(document).ready(function(){

            $(document).ajaxStart(function(){
                $('#div-content').LoadingOverlay("show");
            });

            $(document).ajaxStop(function(){
                $('#div-content').LoadingOverlay("hide");
            });
            
            var cycle = $('#input-cycle').val()
            if(cycle){
                $('#background-cycle').css('background-color','green')
                cycle = JSON.parse(cycle)
                console.log(cycle.stopped_time)
                var cycleStopTime = (cycle.stopped_time) * 1000
                
                var d1 = new Date();
                var d2 = new Date(cycleStopTime);

                startTimer((d2- d1)/1000)

            }else{
                $('#background-cycle').css('background-color','red')
                $('#show-cycle').html('Cycle has stopped');
            }

            function startTimer(duration) {
                var timer = duration, minutes, seconds;
                setInterval(function () {
                    minutes = parseInt(timer / 60, 10);
                    seconds = parseInt(timer % 60, 10);

                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;

                    $('#show-cycle').html('Cycle will end in '+ (minutes + ":" + seconds));

                    if (--timer < 0) {
                        $('#show-cycle').html('Cycle has stopped');
                        $('#background-cycle').css('background-color','red')
                    }
                }, 1000);
            }

            keyboardJS.bind('+',function(e){
                if($('#first-order').is(':focus')){
                    $('#second-order').focus()
                }else if($('#second-order').is(':focus')){
                    $('#amount-order').focus()
                }else{
                    $('#first-order').focus()
                }
            })
            
            keyboardJS.bind('space', function(e){
                $('#add-column-btn').click()
            })

            keyboardJS.bind('l', function(e) {
                e.preventDefault()
                toggle4PCheck()
            });

            keyboardJS.bind('ctrl > enter', function(e) {
                $('#btn-submit').click();
            });

            keyboardJS.bind('n', function(e) {
                $('#next-btn').click();
            });

            keyboardJS.bind('p', function(e) {
                $('#previous-btn').click();
            });

            keyboardJS.bind('ctrl > 1', function(e) {
                $('#first-order').focus()
            });

            keyboardJS.bind('ctrl > 2', function(e) {
                $('#second-order').focus()
            });

            ['a', 'b', 'c', 'd'].forEach((key) => {
                keyboardJS.bind(key, function(e) {
                    var _this = $(`.form-check:has(#input-post-${key.toUpperCase()})`).find('.checkbox');
                    togglePrizeCheck(_this)
                });
            })

            $('.form-check').click(function(){
                var _this = $(this).find('.checkbox');
                togglePrizeCheck(_this)
                if(_this.data('post') === '4P'){
                    toggle4PCheck()
                }
            })

            $('#previous-btn').click(function(){
                const countTable = $('.table').length
                const activeTable = $('.table.active')
                const activeTableIndex = activeTable.data('index')
                const nextTable = $('#table-'+(parseInt(activeTableIndex)-1))
                active_column = parseInt(activeTableIndex)-1

                activeTable.removeClass('active')

                if(active_column < 0){
                    $('#table-'+(countTable-1)).addClass('active')
                    active_column = countTable - 1
                }else{
                    nextTable.addClass('active')
                }
            })

            $('#add-column-btn').click(function(){
                const countTable = $('.table').length
                $('#table-panel').append(`
                    <div class="table-bet">
                        <div class="table-responsive">
                            <table class="table table-borderless" id="table-${ countTable  }" data-index="${ countTable  }">
                                <thead>
                                    <tr><th class="text-center pb-0"><span class="h6 mb-0">Column ${ countTable + 1  }</span></th></tr>
                                    <tr>
                                        <th id="header_${ countTable  }">
                                            <span>2D:</span> <span>0</span><br>
                                            <span>3D:</span> <span>0</span><br>
                                            <span>5D:</span> <span>0</span><br>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody id="column_${ countTable }"></tbody>
                            </table>
                        </div>
                    </div>
                `)
            })

            $('#next-btn').click(function(){
                const countTable = $('.table').length
                const activeTable = $('.table.active')
                const activeTableIndex = activeTable.data('index')
                const nextTable = $('#table-'+(parseInt(activeTableIndex)+1))
                active_column = parseInt(activeTableIndex)+1

                activeTable.removeClass('active')
 
                if(active_column > countTable - 1){
                    console.log('do')
                    $('#table-0').addClass('active')
                    active_column = 0
                }else{
                    console.log('did')
                    nextTable.addClass('active')
                }
            })

            function togglePrizeCheck(_this){
                var isChecked = _this[0].hasAttribute("checked")
                var prizes = JSON.parse(postDataSelector.val())
                if(isChecked) {
                    _this.removeAttr('checked')
                    $(document).ready(function() { // dont remove this
                        _this.parent().removeClass()
                    });
                    prizes.splice(prizes.indexOf(_this.data('post')),1)
                } else {
                    _this.attr('checked','')
                    $(document).ready(function() { // dont remove this
                        _this.parent().addClass('checked')
                    });
                    prizes.push(_this.data('post'))
                }

                if(prizes.length >= 4) {
                    $('#input-post-4P').attr('checked','')
                    $('#input-post-4P').parent().addClass('checked')
                }else{
                    $('#input-post-4P').removeAttr('checked','')
                    $('#input-post-4P').parent().removeClass()
                }

                if(_this.data('post') !== '4P'){
                    postDataSelector.val(JSON.stringify(prizes))
                }
            }

            function toggle4PCheck(){
                var prizes = JSON.parse(postDataSelector.val())
                var constant_prizes = ['A','B','C','D']
                if(prizes.length == 4){
                    for(let i=0; i< constant_prizes.length; i++){
                        var post = $(`.form-check:has(#input-post-${constant_prizes[i]})`).find('.checkbox')
                        post.removeAttr('checked')
                        post.parent().removeClass()
                    }
                    $('#input-post-4P').removeAttr('checked','')
                    $('#input-post-4P').parent().removeClass()

                    postDataSelector.val(JSON.stringify([]))
                }else{
                    for(let i=0; i< constant_prizes.length; i++){
                        if(!prizes.includes(constant_prizes[i])){
                            var post = $(`.form-check:has(#input-post-${constant_prizes[i]})`).find('.checkbox')
                            post.attr('checked','')
                            post.parent().addClass('checked')
                        }
                    }

                    $('#input-post-4P').attr('checked','')
                    $('#input-post-4P').parent().addClass('checked')

                    postDataSelector.val(JSON.stringify(constant_prizes))
                }
            }

        })

    </script>
    <script src="{{asset('admin/assets/js/5d-betting.js')}}"></script>
    <script src="{{asset('admin/assets/js/5d-function.js')}}"></script>
    <script src="{{asset('admin/assets/js/5d-betting-check-input.js')}}"></script>

@endsection
