@extends('admin.layouts.master')

@php 
    $auth = session()->get('auth');
    $lv = $auth->level; 
    $parent_id = $auth->parent_id;
    $id = $auth->id;
@endphp
@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ $settings['language']['LANG_MENU_THIS_ORDER'] }} - 5D</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        {{-- <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <button class="btn btn-link btn-primary btn-sm text-white" id="downloadExcelButton" 
                        data-route="{{route('orders.temp.download')}}" 
                        data-toggle="modal" data-target="#downlod_modal">
                    <i class="icon-file-excel text-white mr-1"></i> {{ $settings['language']['LANG_LABEL_DOWN'] }}
                </button>
            </div>
        </div> --}}
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_THIS_ORDER'] }}</span>
                <span class="breadcrumb-item active">Current</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">
    <div class="breadcrumb">
        @if(isset($topLevelUsers) && count($topLevelUsers) > 0)
            <a href="{{route('orders.temp.index')}}" class="breadcrumb-item pt-0">All</a>
            @foreach($topLevelUsers as $index => $row)
                @if(($index != count($topLevelUsers) - 1 || request()->has('user_id'))  && request()->level >= $lv && $row['level'] >= $lv) 
                    @if($row['user_type'] == 2)
                        <a href="{{route('orders.temp.index').'?level='.$row['level'].'&parent_id='.$row['id']}}" class="breadcrumb-item  pt-0">{{ getUserLevelTitle($row['level']) }} {{ $row['username'] }}</a>
                        @if($index == count($topLevelUsers) - 1 && request()->has('user_id'))
                            <span class="breadcrumb-item active pt-0">{{ getUserLevelTitle($row['level'] + 1) }} {{ $row['username'] }}</span>
                        @endif
                    @else 
                        <span class="breadcrumb-item active pt-0"> {{ $settings['language']['LANG_MENU_DIRECT_USER'] }} {{ $row['username'] }}</span>
                    @endif
                @else
                    <span class="breadcrumb-item active pt-0">{{ getUserLevelTitle($row['level']) }} {{ $row['username'] }}</span>
                @endif
            @endforeach
        @else 
            <span class="breadcrumb-item active pt-0">{{ $settings['language']['LANG_LABEL_ALL'] }}</span>
        @endif
        
    </div>
    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered">
                @if(isset($showTicketDetail) && !$showTicketDetail)
                    <thead>
                        <tr class="bg-slate-800">
                            <th>{{ $settings['language']['LANG_LABEL_USERNAME'] }}</th>
                            <th>{{ $settings['language']['LANG_LABEL_MEM_NUM'] }}</th>
                            <th>{{ $settings['language']['LANG_LABEL_ORDER_NUM'] }}</th>
                            <th colspan="2">{{ $settings['language']['LANG_LABEL_AMOUNT'] }}</th>
                            <th colspan="2">{{ $settings['language']['LANG_LABEL_REBATE'] }}</th> 	 		
                        </tr>
                    </thead>
                    
                    <tbody>
                        @if(isset($users) && count($users) > 0)
                            @if(isset($parentHasOrder) && $parentHasOrder)
                                <tr class="font-weight-bold bg-teal-300">
                                    <td colspan="7">This User</td>
                                </tr>
                                @foreach($users as $index => $row)
                                    @if($row['user_type'] == 2)
                                        @if($row['user']['id'] == request()->parent_id)
                                            <tr>
                                                <td>
                                                    @if($row['hasChildren'])
                                                        <a href="{{route('orders.temp.index').'?level='.$row['user']['username'].'&parent_id='.$row['user']['id']}}">{{ $row['user']['username'] }}</a>
                                                    @else 
                                                        <a href="{{route('orders.temp.index').'?level='.$row['user']['username'].'&parent_id='.$row['user']['id'].'&user_id='.$row['user']['id']}}">{{ $row['user']['username'] }}</a>
                                                    @endif
                                                </td>
                                                <td> {{ $row['memberAmount'] }}</td>
                                                <td>{{ $row['orderAmount'] }}</td>
                                                <td>(R) {{ $row['amountR'] }}</td>
                                                <td>($) {{ $row['amountD'] }}</td>
                                                <td>(R) {{ $row['rebateR'] }}</td>
                                                <td>($) {{ $row['rebateD'] }}</td>
                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                            @endif
                            
                            @if(!$parentHasOrder)
                            <tr class="font-weight-bold bg-teal-300">
                                <td colspan="7">{{ getUserLevelTitle(request()->level + 1) }}</td>
                            </tr>
                            @endif
                            @foreach($users as $index => $row)
                                @if($row['userType'] == 2)
                                    @if($row['user']['id'] != request()->parent_id || !$parentHasOrder)
                                        <tr>
                                            <td>
                                                @if($row['hasChildren'])
                                                    <a href="{{route('orders.temp.index').'?level='.$row['user']['level'].'&parent_id='.$row['user']['id']}}">{{ $row['user']['username'] }}</a>
                                                @else 
                                                    <a href="{{route('orders.temp.index').'?level='.$row['user']['level'].'&parent_id='.$row['user']['id'].'&user_id='.$row['user']['id']}}">{{ $row['user']['username'] }}</a>
                                                @endif
                                            </td>
                                            <td> {{ $row['memberAmount'] }}</td>
                                            <td>{{ $row['orderAmount'] }}</td>
                                            <td>(R) {{ $row['amountR'] }}</td>
                                            <td>($) {{ $row['amountD'] }}</td>
                                            <td>(R) {{ $row['rebateR'] }}</td>
                                            <td>($) {{ $row['rebateD'] }}</td>
                                        </tr>
                                    @endif
                                @endif
                            @endforeach

                            {{-- Direct User --}}
                            @if($hasDirectUser)
                                <tr class="font-weight-bold bg-teal-300">
                                    <td colspan="7">{{ $settings['language']['LANG_MENU_DIRECT_USER'] }}</td>
                                </tr>
                            @endif

                            @foreach($users as $row)
                                @if($row['userType'] == 1)
                                <tr>
                                    <td>
                                        <a href="{{route('orders.temp.index').'?level='.$row['user']['level'].'&parent_id=0&user_id='.$row['user']['id'].'&user_type=1'}}">{{ $row['user']['username'] }}</a>
                                    </td>
                                    <td> {{ $row['memberAmount'] }}</td>
                                    <td>{{ $row['orderAmount'] }}</td>
                                    <td>(R) {{ $row['amountR'] }}</td>
                                    <td>($) {{ $row['amountD'] }}</td>
                                    <td>(R) {{ $row['rebateR'] }}</td>
                                    <td>($) {{ $row['rebateD'] }}</td>
                                </tr>
                                @endif
                            @endforeach
                            {{-- End Direct User --}}

                            @if(isset($total) && count($total) > 0)
                                <tfoot>
                                    <tr>
                                        <th>Total</th>
                                        <th>{{ $total['memberAmount'] }}</th>
                                        <th>{{ $total['orderAmount'] }}</th>
                                        <th>(R) {{ $total['amountR'] }}</th>
                                        <th>($) {{ $total['amountD'] }}</th>
                                        <th>(R) {{ $total['rebateR'] }}</th>
                                        <th>($) {{ $total['rebateD'] }}</th>
                                    </tr>
                                </tfoot>
                            @endif
                        @else 
                            <tr><td colspan="7">No Data</td></tr>
                        @endif
                    </tbody>
                @else 
                    <thead>
                        <tr class="bg-slate-800">
                            <th>{{ $settings['language']['LANG_LABEL_TICKET'] }}</th>
                            <th>{{ $settings['language']['LANG_LABEL_POST_TIME'] }}</th>
                            <th>{{ $settings['language']['LANG_LABEL_PRIZE'] }}</th>
                            <th>{{ $settings['language']['LANG_LABEL_BET'] }}</th>
                            <th colspan="2">{{ $settings['language']['LANG_LABEL_AMOUNT'] }}</th>
                            <th colspan="2">{{ $settings['language']['LANG_LABEL_TICKET_SUM'] }}</th> 	 		
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users['ticket'] as $ticket)
                            @foreach($ticket['orders'] as $index => $row)
                                @if($index == 0)
                                    <tr style="background-color:#eaf9eb">
                                        <td style="vertical-align: top; cursor:pointer" class="show-order-detail" data-toggle="modal" data-target="#order_detail_modal" 
                                            data-route="">{{ $ticket['ticket'] }}</td>
                                        <td style="vertical-align: top;">{{ $ticket['created_at'] }}</td>
                                        <td>{{ $row['prize'] }}</td>
                                        <td>{{ $row['bet'] }}</td>
                                        <td>(R) {{ $row['amountR'] }}</td>
                                        <td>($) {{ $row['amountD'] }}</td>
                                        <td style="vertical-align: top;">(R) {{ $ticket['ticketAmountR'] }}</td>
                                        <td style="vertical-align: top;">($) {{ $ticket['ticketAmountD']  }}</td>
                                    </tr>
                                @else 
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>{{ $row['prize'] }}</td>
                                        <td>{{ $row['bet'] }}</td>
                                        <td>(R) {{ $row['amountR'] }}</td>
                                        <td>($) {{ $row['amountD'] }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @endif
                            @endforeach
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="6"> Total</td>
                            <td>(R) {{ $total['amountR'] }}</td>
                            <td>($) {{ $total['amountD'] }}</td>
                        </tr>
                    </tfoot>
                @endif
            </table>
        </div>
    </div>
</div>
<div id="downlod_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">Download Excel</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body text-center p-4 h5 m-0" id="downloadModalBody">
                <i class='icon-spinner2 spinner'></i> Downloading...
            </div>
        </div>
    </div>
</div>
@endsection
