@extends('admin.layouts.master')

@section('custom-css')
    <style>
        .table.table-cycle tr.active,
        .table.table-cycle tr:hover{
            background-color: #ff7043;
            color: white;
        }

        .table tbody{
            overflow:auto;
            display: block;
            width:100%;
        }
        .table-cycle tbody{
            height:500px;
        }
        .table-ticket tbody{
            height:150px;
        }
        .table-ticket-detail tbody{
            height:130px;
        }
        .table-cycle thead, tbody tr,
        .table-ticket thead, tbody tr,
        .table-ticket-detail thead, tbody tr {
            display:table;
            width:100%;
            table-layout:fixed;
        }
    </style>
@endsection
@section('content')
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">System Betting - 5D</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">5D</span>
                <span class="breadcrumb-item active">Record</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content p-xs-2">
    <div class="row">
        <div class="col-md-4 mb-2">
            <div class="row">
                <div class="col-12">
                    <table class="table table-bordered table-cycle">
                        <thead class="text-center">
                            <th> Date and Time </th>
                        </thead>
                        <tbody>
                            @foreach($recordCycles as $cycle)
                            <tr data-id="{{$cycle['id']}}" class=" text-center " style="cursor:pointer">
                                <td><a href="{{route('5d.record.cycle',$cycle['id'])}}">{{ date('Y-m-d H:i A', $cycle['result_time']) }} </a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        {{-- <div class="col-md-8">
            <div class="ticket-info">
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-ticket">
                                <thead>
                                    <th>Date and Time</th>
                                    <th>Amount</th>
                                    <th>Ticket</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center" colspan="3"> Please select a cycle</td>
                                    </tr>
                                </tbody>
                                <tfoot></tfoot>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="card mt-3" id="ticket-detail">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <p class="" style="margin-bottom: .0!important;"><label>Ticket: &nbsp;</label><span id="ticket"></span></p>
                                <p style="margin-bottom: .0!important;"><label>Total: &nbsp;</label><span id="amount"></span></p>
                                <p style="margin-bottom: .0!important;"><label>Total Win: &nbsp;</label><span id="win-amount"></span></p>
                            </div>
                            <div class="col-6">
                                <p style="margin-bottom: .0!important;"><label>Date: &nbsp;</label><span id="created-at"></span></p>
                                <p style="margin-bottom: .0!important;"><label>Result Time: &nbsp;</label><span id="result-time"></span></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="table-reponsive">
                                    <table class="table table-bordered table-ticket-detail">
                                        <thead>
                                            <th>Post</th>
                                            <th>Number</th>
                                            <th>PC/U</th>
                                            <th>Amount</th>
                                            <th>Win</th>
                                            <th>Status</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="6" class="text-center">Please select a ticket</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div> --}}
    </div>
</div>

@endsection

@section('custom-js')
 <script src="{{asset('admin/assets/js/5d-betting.js')}}"></script>
 <script src="{{asset('admin/assets/js/5d-function.js')}}"></script>
@endsection
