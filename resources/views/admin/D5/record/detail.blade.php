@extends('admin.layouts.master')

@section('custom-css')
    <style>

        table {
            table-layout: fixed;
            width: 100%;
        }
        table td {
            word-wrap: break-word;         /* All browsers since IE 5.5+ */
            overflow-wrap: break-word;     /* Renamed property in CSS3 draft spec */
        }
        .table thead th {
            border-bottom: none
        }
        .table.active{
            border: 2px solid #ff5722
        }
        .table.active thead {
            border-bottom: 2px solid #ff5722
        }
        .table{
            border: 2px solid gray
        }
        .table thead {
            border-bottom: 2px solid gray
        }
        .table-container{
            overflow: hidden;
            overflow-x: scroll;
        }
        .table-bet{
            min-width: 20%;
            max-width: 20%;
            margin-right: 5px;
        }
    </style>
@endsection

@section('content')
<input type="hidden" value="{{ $sheets }}" id="input-sheets"/>
<input type="hidden" value="{{ $cycle }}" id="input-cycle"/>
{!! Form::hidden('win-info',json_encode($win_info),['id' => 'input-win-info']) !!}

<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">System Betting - 5D</span></h4>
        </div>
        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{ route('5d.record.cycle.download',[ 'cycle' => $cycle ]) }}" class="btn btn-primary btn-sm">
                    <i class="icon-file-excel text-white mr-1"></i> Download
                </a>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content p-xs-2">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-4">
                            <p style="margin-bottom: .0!important;"><label>Date: &nbsp;</label><span id="created-at"></span></p>
                            <p style="margin-bottom: .0!important;"><label>Result Time: &nbsp;</label><span id="result-time">{{ date('Y-m-d H:i A', $cycle->result_time) }}</span></p>
                            <p style="margin-bottom: .0!important;"><label>User: &nbsp;</label><span id="user"></span></p>
                        </div>
                        <div class="col-4">
                            <p style="margin-bottom: .0!important;"><label>Total: &nbsp;</label><span id="total">0</span> (R)</p>
                            <p style="margin-bottom: .0!important;"><label>Total 2D: &nbsp;</label><span id="total-2d">0</span> (R)</p>
                            <p style="margin-bottom: .0!important;"><label>Total 3D: &nbsp;</label><span id="total-3d">0</span> (R)</p>
                            <p style="margin-bottom: .0!important;"><label>Total 5D: &nbsp;</label><span id="total-5d">0</span> (R)</p>
                        </div>
                        <div class="col-4">
                            <p style="margin-bottom: .0!important;"><label>Total Win: &nbsp;</label><span id="total-win">0</span> (R)</p>
                            <p style="margin-bottom: .0!important;"><label>Total Win 2D: &nbsp;</label><span id="total-win-2d">0</span> (R)</p>
                            <p style="margin-bottom: .0!important;"><label>Total Win 3D: &nbsp;</label><span id="total-win-3d">0</span> (R)</p>
                            <p style="margin-bottom: .0!important;"><label>Total Win 5D: &nbsp;</label><span id="total-win-5d">0</span> (R)</p>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="data-list m-2">
                        <div class="row">
                            <div class="col-12">
                                <div class="table-container d-flex flex-row flex-nowrap" id="table-panel">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-center">
                    <h4>Sheets</h4>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1">Previous</a>
                            </li>
                            @for($i = 1; $i <= count($sheets); $i++)
                                <li class="page-item"><a class="page-link sheet-page" href="javascript:void(0)" data-id="{{$i-1}}">{{ $i }}</a></li>
                            @endfor
                            <li class="page-item">
                                <a class="page-link" href="#">Next</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js" integrity="sha256-AdQN98MVZs44Eq2yTwtoKufhnU+uZ7v2kXnD5vqzZVo=" crossorigin="anonymous"></script>

<script>
    $(document).ready(function(){

        var sheets = $('#input-sheets').val()
        var cycle = $('#input-cycle').val()
        var win_info = $('#input-win-info').val()
        if(sheets){
            sheets = JSON.parse(sheets)
            cycle = JSON.parse(cycle)
            win_info = JSON.parse(win_info)
        }

        $('.sheet-page').click(function(){
            var index = $(this).attr('data-id')
            show_sheet(index)
        })

        function show_sheet(index){
            var sheet = sheets[index]
            var sheet_win_info = win_info[sheet.id]
            
            console.log(sheet_win_info)
            $('#user').html(sheet.user.username)
            $('#created-at').html(sheet.created_at)
            $('#total-2d').html(sheet.total_amount_2d)
            $('#total-3d').html(sheet.total_amount_3d)
            $('#total-5d').html(sheet.total_amount_5d)
            $('#total').html(sheet.total_amount_2d + sheet.total_amount_3d + sheet.total_amount_5d);
            $('#total-win-2d').html(sheet_win_info.total_win_2d)
            $('#total-win-3d').html(sheet_win_info.total_win_3d)
            $('#total-win-5d').html(sheet_win_info.total_win_5d)
            $('#total-win').html(sheet_win_info.total_win_amount)
            $('#table-panel').html(``)


            var columns = sheet.order_page_columns
            console.log(columns)

            for(let i=0; i<columns.length; i++){

                var count = {}
                count['2D'] = 0
                count['3D'] = 0
                count['5D'] = 0

                var row = ``;
                var bet_content = JSON.parse(columns[i].bet_content)

                for(let j=0; j<bet_content.length; j++){

                    count[bet_content[j].bet_type] += bet_content[j].single_amount * bet_content[j].quantity

                    row += `
                    <tr><td>
                        <p><strong> ${ bet_content[j]['format_number'] }=${ bet_content[j].single_amount * bet_content[j].quantity } (${ bet_content[j].prize })</strong>
                        </p>
                    </td></tr>
                    `
                }
                

                $('#table-panel').append(
                    `<div class="table-bet">
                        <div class="table-responsive">
                            <table class="table table-borderless active">
                                <thead>
                                    <tr><th class="text-center pb-0"><span class="h6 mb-0">Column ${ i + 1 }</span></th></tr>
                                    <tr>
                                        <th>
                                            <span>2D:</span> <span>${ count['2D'] }</span><br>
                                            <span>3D:</span> <span>${ count['3D'] }</span><br>
                                            <span>5D:</span> <span>${ count['5D'] }</span><br>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    ${ row }    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    `
                )
            }
        }
    })
</script>
@endsection
