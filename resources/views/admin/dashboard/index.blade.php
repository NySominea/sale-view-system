@extends('admin.layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Dashboard</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <span class="breadcrumb-item active"><i class="icon-home2 mr-2"></i> Dashboard</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <h1 class="text-center mb-4">Welcome to Sethei 5D & Ball Lottery Sale Management</h1>
</div>

@endsection

@section('page-script')
<script src="{{asset('admin/global_assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
<script src="{{asset('admin/global_assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
<script src="/admin/global_assets/js/demo_pages/dashboard.js"></script>
@endsection