<nav class="bg-light border-bottom" id="top-nav">
    <div class="container">
        <div class="text-right nav-right">
            <div class="row m-0">
                <div class="col-lg-3 col-md-4 col-sm-5 p-0 text-left slogan">
                    <span class="text-muted">{{ isset($companyInfo[$constant['COMPANY']['SLOGAN']]) ? $companyInfo[$constant['COMPANY']['SLOGAN']]->value : ''  }}</span>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-7 p-0 nav-item-list">
                    <ul class="p-0">
                        <li class="text-muted pr-3 border-right border-secondary d-none d-lg-inline">
                            <i class="fa fa-phone-square"></i> {{isset($companyInfo[$constant['COMPANY']['PHONE']]) ? $companyInfo[$constant['COMPANY']['PHONE']]->value : ''}}
                        </li>
                        <li class="text-muted pl-3 pr-3 border-right border-secondary">
                            <a class="text-muted" href="{{isset($setting[$constant['SETTING']['FACEBOOK']]) ? $setting[$constant['SETTING']['FACEBOOK']]->value : ''}}" target="_blank"><i class="fa fa-facebook-square" data-toggle="tooltip" data-placement="bottom" title="Facebook"></i></a>
                            <a class="text-muted pl-2" href="{{isset($setting[$constant['SETTING']['YOUTUBE']]) ? $setting[$constant['SETTING']['YOUTUBE']]->value : ''}}" target="_blank"><i class="fa fa-youtube" data-toggle="tooltip" data-placement="bottom" title="Youtube"></i></a>
                        </li>
                        <li class="text-muted pl-3 pr-3 border-right border-secondary d-inline">
                            <a class="text-muted" data-toggle="modal" data-target="#qrModal" href="#" onclick="event.preventDefault();" ><i class="fa fa-qrcode" data-toggle="tooltip" data-placement="bottom" title="QR Code"></i></a>
                        </li>
                        <li class="text-muted pl-3 pr-3 border-right border-secondary d-inline">
                            <span class="fb-share-button" style="top:-2px" 
                                data-href="{{ url('/') }}" 
                                data-layout="button_count">
                            </span>
                        </li>
                        <li class="text-muted pl-3">
                            <a href="#" class="text-muted text-decoration-none" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{asset('client/asset/image/'.app()->getLocale().'.png')}}">
                                <span class="d-inline d-sm-none d-md-inline">
                                    @if(app()->getLocale() == 'en') English
                                    @elseif(app()->getLocale() == 'kh')  ភាសាខ្មែរ
                                    @elseif(app()->getLocale() == 'zh')  中文
                                    @endif
                                </span>  
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <div class="dropdown-menu  dropdown-menu-right" aria-labelledby="dropdownMenuButton" style="z-index:99999;min-width:8.5rem">
                                @if(app()->getLocale() != 'kh') 
                                <a class="dropdown-item" href="{{route('setLocale','kh')}}"><img src="{{asset('client/asset/image/kh.png')}}"> ភាសាខ្មែរ</a>
                                @endif
                                @if(app()->getLocale() != 'en') 
                                    <a class="dropdown-item" href="{{route('setLocale','en')}}"><img src="{{asset('client/asset/image/en.png')}}"> English</a>
                                @endif
                                @if(app()->getLocale() != 'zh')
                                <a class="dropdown-item" href="{{route('setLocale','zh')}}"><img src="{{asset('client/asset/image/zh.png')}}"> 中文</a>
                                @endif
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
<nav id="myNav" class="navbar navbar-expand-lg navbar-light w-100 fixed-top-absolute">
    <div class="container" >
        <a class="navbar-brand animated fadeIn" href="{{route('home.index')}}">
            @php $image = isset($setting[$constant['SETTING']['BW_LOGO']]) ? $setting[$constant['SETTING']['BW_LOGO']]->getMedia('images')->first() : null @endphp
            <img width="40px" src="{{asset($image ? $image->getUrl() : '')}}" alt="Sethei Lottery">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNavbar">
        <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link" href="{{route('home.index')}}">{{trans('client.home')}}</a></li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                        {{trans('client.products')}}
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{route('product.5d')}}">{{trans('client.5d')}}</a>
                        <a class="dropdown-item" href="{{route('product.lotto')}}">{{trans('client.lotto')}}</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                        {{trans('client.game_result')}}
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{route('product.5d')}}">{{trans('client.5d')}}</a>
                        <a class="dropdown-item" href="{{route('product.lotto')}}">{{trans('client.lotto')}}</a>
                    </div>
                </li>
                <li class="nav-item"><a class="nav-link" href="{{route('activity.index')}}">{{trans('client.activities')}}</a></li>
                <li class="nav-item"><a class="nav-link" href="{{route('faq.index')}}">{{trans('client.faq')}}</a></li>
                <li class="nav-item"><a class="nav-link" href="{{route('career.index')}}">{{trans('client.careers')}}</a></li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle pr-0" href="#" id="navbardrop" data-toggle="dropdown">
                        {{trans('client.company')}}
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{route('company.about')}}">{{trans('client.about_us')}}</a>
                        <a class="dropdown-item" href="{{route('company.contact')}}">{{trans('client.contact_us')}}</a>
                        <a class="dropdown-item" href="{{route('company.shop')}}">{{trans('client.shop_location')}}</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="modal fade" id="qrModal" tabindex="-1" role="dialog" aria-labelledby="qrModalLabel" aria-hidden="true" style="top:15%;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Scan to Visit Us</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                @if(isset($setting[$constant['SETTING']['SITE_URL']]) && $setting[$constant['SETTING']['SITE_URL']]->value)
                        {!! QrCode::size(250)->generate($setting[$constant['SETTING']['SITE_URL']]->value); !!}
                @else
                    QR Code not available at the moment!
                @endif
            </div>
        </div>
    </div>
</div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
