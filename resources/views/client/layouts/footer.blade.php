<footer style="background:black">
    <div id="footer" class="container pt-3 pb-3">
        <div class="row text-center">
            <div class="col-md-4 mb-3">
                <p class="title h6"><b> {{trans('client.about')}} {{trans('client.sethei_lottery')}}</b></p>
                @php $image = isset($setting[$constant['SETTING']['MAIN_LOGO']]) ? $setting[$constant['SETTING']['MAIN_LOGO']]->getMedia('images')->first() : null @endphp
                {{-- <div class="text-center"><img width="30%" alt="Sethei Lottery" src="{{asset($image ? $image->geturl() : '')}}"></div> --}}
                <div class="text-center"><img width="30%" alt="Sethei Lottery" src="/client/asset/image/logo.png"></div>
                <p class="mb-1 mt-1">{!! isset($companyInfo[$constant['COMPANY']['VISION']]->value) ? $companyInfo[$constant['COMPANY']['VISION']]->value : ''  !!}</p>
            </div>
            <div class="col-md-4 mb-3">
                <p class="title h6"><i class="fa fa-home"></i><b> {{trans('client.office')}} {{trans('client.sethei_lottery')}}</b></p>
                <p class="mb-1">{!! isset($companyInfo[$constant['COMPANY']['VISION']]) ? $companyInfo[$constant['COMPANY']['VISION']]->value : ''  !!}</p>
                <p class="mb-1"><i class="fa fa-phone"></i> {{ isset($companyInfo[$constant['COMPANY']['PHONE']]) ? $companyInfo[$constant['COMPANY']['PHONE']]->value : ''  }} </p>
                <p class="mb-2"><i class="fa fa-envelope"></i> {{ isset($companyInfo[$constant['COMPANY']['PHONE']]) ? $companyInfo[$constant['COMPANY']['EMAIL']]->value : ''  }}</p>

                {!! isset($setting[$constant['SETTING']['SMALL_MAP']]) ? $setting[$constant['SETTING']['SMALL_MAP']]->value : ''  !!}
            </div>
            <div class="col-md-4 mb-3">
                <p class="title h6"><b> {{trans('client.follow_us')}} </b></p>
                <div class="social">
                    <a class="facebook rounded-circle" href="{{ isset($setting[$constant['SETTING']['FACEBOOK']]) ? $setting[$constant['SETTING']['FACEBOOK']]->value : ''  }}" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a class="instagram rounded-circle" href="{{ isset($setting[$constant['SETTING']['YOUTUBE']]) ? $setting[$constant['SETTING']['YOUTUBE']]->value : ''  }}" target="_blank"><i class="fa fa-youtube"></i></a>
                </div>
                @if(isset($setting[$constant['SETTING']['ANDROID_5D_LINK']]) && $setting[$constant['SETTING']['ANDROID_5D_LINK']]->value)
                    <p class="title h6"><b>{{trans('client.our_product')}}</b></p>
                    <span class="mr-4">{{trans('client.5d_game')}}</span>  <a href="{{ $setting[$constant['SETTING']['ANDROID_5D_LINK']]->value }}" target="_blank"><img width="120px" height="40px" class="text-right" src="{{asset('client/asset/image/googleplay.png')}}" alt="Playstore"><br></a>
                @endif
            </div>
        </div>
    </div>

    <div id="poweredBy" class="text-white text-center my-auto p-3">
        <div class="container-fluid h6 m-0">
            Powered by <span class="text-white"><b>{{ isset($companyInfo[$constant['COMPANY']['POWERED_BY']]) ? $companyInfo[$constant['COMPANY']['POWERED_BY']]->value : ''  }}</b></span>
        </div>
    </div>
</footer>