<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<meta property="og:url"           content="{{ url('/') }}" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="{{ isset($setting[$constant['SETTING']['SITE_NAME']]) ? $setting[$constant['SETTING']['SITE_NAME']]->value : '' }}" />
<meta property="og:description"   content="{!! isset($setting[$constant['SETTING']['SEO_DESCRIPTION']]) ? $setting[$constant['SETTING']['SEO_DESCRIPTION']]->value : '' !!}" />

@if(isset($setting[$constant['SETTING']['MAIN_LOGO']]))
    @php $image = $setting[$constant['SETTING']['MAIN_LOGO']]->getMedia('images')->first() @endphp
    <meta property="og:image" content="{{ $image ? $image->getUrl() : '/client/asset/image/logo.png' }}" />
    <link rel="icon" href="{{ $image ? $image->getUrl() : ''  }}">
@endif

<title>@yield('title') | {{ isset($setting[$constant['SETTING']['SITE_NAME']]) ? $setting[$constant['SETTING']['SITE_NAME']]->value : '' }}</title>

@yield('meta-tag')
<meta property="og:site_name" content="{{ isset($setting[$constant['SETTING']['SITE_NAME']]) ? $setting[$constant['SETTING']['SITE_NAME']]->value : '' }}">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<link href="https://fonts.googleapis.com/css?family=Koulen" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Dangrek" rel="stylesheet">
<link rel="stylesheet" href="{{asset('client/vendor/odometer/css/odometer-theme-default.css')}}" />
<link rel="stylesheet" href="{{asset('client/css/custom.css')}}">
<link rel="stylesheet" href="{{asset('client/css/index.css')}}">

<style>
    @font-face {
        font-family: 'Khmer OS Battambong';
        src: url('/client/asset/font/KhmerOS_battambang.ttf');
    }
    @font-face {
        font-family: 'Poppins';
        src: url('/client/asset/font/Poppins-Regular.ttf');
    }
    @font-face {
        font-family: f_cate;
        src: {{ app()->getLocale() == 'en' ? "url(/client/asset/font/Poppins-Regular.ttf)" : "url(/client/asset/font/KhmerOS_battambang.ttf)" }};
    }
    @font-face {
        font-family: f_logo;
        src: url('/client/asset/font/NiDAAngkor.ttf');
    }
</style>

@yield('custom-css')

<script src="https://unpkg.com/scrollreveal@4.0.0/dist/scrollreveal.min.js"></script>