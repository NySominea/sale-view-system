<!DOCTYPE html>
<html lang="en">
<head>
    @include('client.layouts.head')
</head>

<body>
    @include('client/layouts.header')
    <section>
        @yield('content')
    </section>
    @include('client/layouts.footer')
    <div id="btnScrollUp">
        <button class="btn" style=""><i class="fa fa-arrow-up"></i></button>
    </div>
    <input type="hidden" value="{{url('/')}}" id="baseUrl">
</body>
@include('client.layouts.script')
@yield('custom-js')
</html>