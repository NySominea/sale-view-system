<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderPageColumnTempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_page_column_temps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_page_id')->index();
            $table->integer('column');
            $table->unsignedBigInteger('order_id');
            $table->double('total_amount_2d')->default(0.0);
            $table->double('total_amount_3d')->default(0.0);
            $table->double('total_amount_5d')->default(0.0);
            $table->double('total_win_amount')->default(0.0);
            $table->double('total_win_2d')->default(0.0);
            $table->double('total_win_3d')->default(0.0);
            $table->double('total_win_5d')->default(0.0);
            $table->longText('bet_content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_page_column_temps');
    }
}
