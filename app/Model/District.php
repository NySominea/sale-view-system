<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class District extends Eloquent
{
	protected $fillable = [
        'id',
        'name',
        'code',
        'province_id'
    ];

    public function province(){
        return $this->belongsTo(Province::class);
    }
}
