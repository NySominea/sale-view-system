<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class LanguageAdmin extends Eloquent
{

	protected $fillable = [
		'id',
        'module',
        'english',
        'chinese',
        'khmer',
        'label',
        'abstract',
	];
}
