<?php

namespace App\Model\D5;

use Illuminate\Database\Eloquent\Model;

class GameRebate extends Model
{
    protected $table = "d_game_rebates";

    protected $fillable = [
            'id',
            'category_id',
            'root_id',
            'l1_rebate',
            'l2_rebate',
            'l3_rebate',
            'l4_rebate',
            'l5_rebate',
            'l6_rebate',
            'l7_rebate',
            'l8_rebate'
    ];
    
    public function category(){
        return $this->belongsTo(GameCategory::class,'category_id');
    }
}
