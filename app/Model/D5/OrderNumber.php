<?php

namespace App\Model\D5;

use Illuminate\Database\Eloquent\Model as Eloquent;

class OrderNumber extends Eloquent
{
	protected $table = "d_order_numbers";

    protected $fillable = [
        'id',
        'prize',
        'type',
        'cycle_id',
        'number',
        'amount',
        'is_win'
    ];
}
