<?php

namespace App\Model\D5;

use Illuminate\Database\Eloquent\Model as Eloquent;

class OrderBetTemp extends Eloquent
{
	protected $table = "d_order_bet_temps";

    protected $fillable = [
        'id',
        'ticket',
        'prize',
        'bet',
        'unit',
        'amount',
        'type',
        'is_win',
        'win_amount', 
    ];
}
