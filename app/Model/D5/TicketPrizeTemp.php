<?php

namespace App\Model\D5;

use Illuminate\Database\Eloquent\Model as Eloquent;

class TicketPrizeTemp extends Eloquent
{
	protected $table = "d_ticket_prize_temps";

    protected $fillable = [
        'id',
        'user_id',
        'order_id',
        'cycle_id',
		'ticket',
		'number',
		'win_amount',
		'type',
		'prize',
		'is_win'
    ];
}
