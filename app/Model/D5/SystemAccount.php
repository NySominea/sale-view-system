<?php

namespace App\Model\D5;

use Illuminate\Database\Eloquent\Model as Eloquent;

class SystemAccount extends Eloquent
{
	protected $table = "d_system_accounts";
    
    protected $fillable = [
                    'id',
                    'type_id',
                    'category_id',
                    'title',
                    'number',
                    'name',
                    'balance',
                    'sort',
                    'state'
                ];

    public function type(){
        return $this->belongsTo(SystemAccountType::class,'type_id','id');
    }
    public function category(){
        return $this->belongsTo(SystemAccountCategory::class,'category_id','id');
    }
}
