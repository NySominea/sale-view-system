<?php

namespace App\Model\D5;

use Illuminate\Database\Eloquent\Model as Eloquent;

class OrderNumberTemp extends Eloquent
{
	protected $table = "d_order_number_temps";

    protected $fillable = [
        'id',
        'prize',
        'type',
        'cycle_id',
        'number',
        'amount',
        'is_win'
    ];
}
