<?php
namespace App\Model\D5;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ManagerLog extends Eloquent
{
	protected $table = 'd_manager_logs';

    protected $fillable = [
        'id',
        'manager_id',
        'content',
    ];

    protected $casts = [
        'content' => 'array'
    ];

    public function manager(){
        return $this->belongsTo(Manager::class);
    }

    public static function createManagerLog($userId = null, $arr){
        return ManagerLog::create([
            'manager_id' => $userId,
            'content' => $arr
        ]);
    }
}
