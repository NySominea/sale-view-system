<?php

namespace App\Model\D5;

use Illuminate\Database\Eloquent\Model as Eloquent;

class UserAccountLog extends Eloquent
{
	protected $table = "d_user_account_logs";

    protected $fillable = [
            'id',
            'user_id',
            'account_id',
            'log_type',
            'is_transfer',
            'amount',
            'balance',
            'commission',
            'win_money',
            'to_type',
            'to_user_id',
            'to_account_id',
            'abstract',
            'manager_id',
            'log_number',
            'created_at',
            'updated_at'
    ];

    public function account(){
        return $this->belongsTo(UserAccount::class);
    }
    public function user(){
        return $this->belongsTo(\App\User::class);
    }
    public function toUser(){
        return $this->belongsTo(\App\User::class,'to_user_id','id');
    }
    public function manager(){
        return $this->belongsTo(\App\Model\Manager::class,'manager_id','id');
    }
    
    public static function generateLogNumber($logType, $userId){
        return $logType.addPrefixStringPad($userId,4,'0').date('Y').date('m').date('d').date('H').date('i').date('s');
    }
}
