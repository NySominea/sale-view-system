<?php

namespace App\Model\D5;

use Illuminate\Database\Eloquent\Model as Eloquent;

class UserAccount extends Eloquent
{
	protected $table = "d_user_accounts";

    protected $fillable = [
        'id',
        'user_id',
        'type_id',
        'category_id',
        'title',
        'number',
        'name',
        'balance',
        'frozen',
        'state',
        'sort',
        'created_at',
        'updated_at'
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public static function createUserCashAccount($userId = null){
        $userId = $userId ?? auth()->id();
        return UserAccount::create([
            'title' => 'Default Account',
            'number' => 'Default Account',
            'name' => 'Default Account',
            'balance' => 0,
            'frozen' => 0,
            'state' => 1,
            'user_id' => $userId,
            'type_id' => UserAccountConstant::CASH_ACCOUNT
        ]);
    }
    public static function createUserProfitAccount($userId = null){
        $userId = $userId ?? auth()->id();
        return UserAccount::create([
            'title' => 'Profit Account',
            'number' => 'Profit Account',
            'name' => 'Profit Account',
            'balance' => 0,
            'frozen' => 0,
            'state' => 1,
            'user_id' => $userId,
            'type_id' => UserAccountConstant::PROFIT_ACCOUNT
        ]);
    }
}
