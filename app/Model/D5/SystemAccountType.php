<?php

namespace App\Model\D5;

use Illuminate\Database\Eloquent\Model;

class SystemAccountType extends Model
{
    protected $table = "d_system_account_types";

    protected $fillable = [
            'id',
            'name'
     ];
}
