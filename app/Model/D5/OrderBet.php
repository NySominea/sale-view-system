<?php
namespace App\Model\D5;

use Illuminate\Database\Eloquent\Model as Eloquent;

class OrderBet extends Eloquent
{
	protected $table = "d_order_bets";

    protected $fillable = [
           'id',
           'ticket',
           'prize',
           'bet',
           'unit',
           'amount',
           'type',
           'is_win',
           'win_amount'
        ];
}
