<?php

namespace App\Model\D5;

use Illuminate\Database\Eloquent\Model as Eloquent;

class TicketPrize extends Eloquent
{
	protected $table = "d_ticket_prizes";

    protected $fillable = [
        'id',
        'user_id',
        'order_id',
        'cycle_id',
		'ticket',
		'number',
		'win_amount',
		'type',
		'prize',
		'is_win'
    ];
}
