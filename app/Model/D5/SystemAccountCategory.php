<?php
namespace App\Model\D5;

use Illuminate\Database\Eloquent\Model as Eloquent;

class SystemAccountCategory extends Eloquent
{
	protected $table = "d_system_account_categories";
    
    protected $fillable = [
            'id',
            'name',
            'sort',
            'type_id',
    ];

    public function type(){
        return $this->belongsTo(SystemAccountType::class,'type_id','id');
    }
}
