<?php

namespace App\Model\D5;

use Illuminate\Database\Eloquent\Model as Eloquent;
use App\User;

class Order extends Eloquent
{
	protected $table = 'd_orders';

	protected $fillable = [
		'id',
		'parent_id',
		'cycle_id',
		'user_id',
		'is_settle',
		'is_win',
		'win_amount',
		'win_2d_amount',
		'win_3d_amount',
		'win_5d_amount',
		'ticket',
		'amount',
		'bet',
		'bet_content',
		'l1_id',
		'l2_id',
		'l3_id',
		'l4_id',
		'l5_id',
		'l6_id',
		'l7_id',
		'l8_id',
		'l1_rebate',
		'l2_rebate',
		'l3_rebate',
		'l4_rebate',
		'l5_rebate',
		'l6_rebate',
		'l7_rebate',
		'l8_rebate',
		'l1_du',
		'l2_du',
		'l3_du',
		'l4_du',
		'l5_du',
		'l6_du',
		'l7_du',
		'l8_du',
		'user_type',
		'state',
];

	public function cycle(){
		return $this->belongsTo(Cycle::class);
	}

	public function user(){
		return $this->belongsTo(User::class);
	}

	public function parent($lvid = null){
		return $lvid ? $this->belongsTo(User::class,'l'.$lvid.'_id','id')->first() : $this->belongsTo(User::class,'parent_id','id');
	}

	public function orderBet(){
		return $this->hasMany(OrderBet::class,'ticket','ticket');
	}


	protected $casts = [
		'bet' => 'array',
		'bet_content' => 'array',
	];
}
