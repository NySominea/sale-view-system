<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class LanguageApi extends Eloquent
{
	protected $fillable = [
        'id',
        'module',
        'english',
        'khmer',
        'chinese',
        'label',
        'abstract'
    ];
}
