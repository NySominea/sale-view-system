<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Exchange extends Eloquent
{
	protected $table = 'exchanges';

	protected $fillable = [
		'id',
        'from_currency',
        'to_currency',
        'rate',
	];
}
