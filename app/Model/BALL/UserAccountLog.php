<?php

namespace App\Model\BALL;

use Illuminate\Database\Eloquent\Model as Eloquent;
use App\User;

class UserAccountLog extends Eloquent
{
	protected $table = "ball_user_account_logs";

    protected $fillable = [
        'id',
        'log_number',
        'amount',
        'balance',
        'commission',
        'win_money',
        'is_transfer',
        'log_type',
        'abstract',
        'to_type',
        'user_id',
        'account_id',
        'to_user_id',
        'to_account_id',
        'manager_id',
    ];

    public function account(){
        return $this->belongsTo(\App\Model\BALL\UserAccount::class);
    }
    public function user(){
        return $this->belongsTo(\App\User::class);
    }
    public function toUser(){
        return $this->belongsTo(\App\User::class,'to_user_id','id');
    }
    public function manager(){
        return $this->belongsTo(\App\Model\Manager::class,'manager_id','id');
    }
    
    public static function generateLogNumber($logType, $userId){
        return $logType.addPrefixStringPad($userId,4,'0').date('Y').date('m').date('d').date('H').date('i').date('s');
    }
}
