<?php

namespace App\Model\BALL;

use Illuminate\Database\Eloquent\Model;

class SystemAccountType extends Model
{
    protected $table = "ball_system_account_types";
    protected $fillable = [
        'id',
        'name'
    ];
}
