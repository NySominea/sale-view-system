<?php

namespace App\Model\BALL;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Promotion extends Eloquent
{
	protected $table = "ball_promotions";
    protected $fillable = [
        'id',
        'name',
        'from_date',
        'to_date',
        'target_order',
        'bonus_order',
        'running_text'
    ];
}
