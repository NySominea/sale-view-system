<?php

namespace App\Model\BALL;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Order extends Eloquent
{
	protected $table = 'ball_orders';
	protected $fillable = [
        'id',
        'ticket',	
        'is_settle',
        'is_win',
        'win_prize',
        'win_number',
        'win_amount',
        'amount',
        'no1',
        'no2',
        'no3',
        'no4',
        'no5',
        'no6',	
        'l1_id',
        'l2_id',
        'l3_id',
        'l4_id',
        'l5_id',
        'l6_id',
        'l7_id',
        'l8_id',
        'l1_rebate',
        'l2_rebate',
        'l3_rebate',
        'l4_rebate',
        'l5_rebate',
        'l6_rebate',
        'l7_rebate',
        'l8_rebate',
        'l1_du',
        'l2_du',
        'l3_du',
        'l4_du',
        'l5_du',
        'l6_du',
        'l7_du',
        'l8_du',
        'user_type',
        'parent_id',
        'state',
        'cycle_id',
        'user_id',
        'game_type_id',
        'is_test'
    ];

    public function cycle(){
        return $this->belongsTo(\App\Model\BALL\Cycle::class);
    }

    public function user(){
        return $this->belongsTo(\App\User::class);
    }

    public function parent($lvid = null){
        return $lvid ? $this->belongsTo(\App\User::class,'l'.$lvid.'_id','id')->first() : $this->belongsTo(\App\User::class,'parent_id','id');
    }

    protected $casts = [
        'win_number' => 'array',
    ];
			
}
