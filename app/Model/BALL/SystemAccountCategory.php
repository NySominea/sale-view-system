<?php
namespace App\Model\BALL;

use Illuminate\Database\Eloquent\Model as Eloquent;

class SystemAccountCategory extends Eloquent
{
	protected $table = "ball_system_account_categories";
    protected $fillable = [
        'id',
        'name',
        'sort',
        'type_id',
    ];

    public function type(){
        return $this->belongsTo(SystemAccountType::class);
    }
}
