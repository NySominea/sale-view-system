<?php

namespace App\Model\BALL;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Cycle extends Eloquent
{
	protected $table = 'ball_cycles';

	protected $fillable = [
        'id',
        'cycle_sn',
        'state',
        'has_released',
        'stopped_time',
        'result_time',
        'result_number',
        'is_jackpot',
        'prize',
        'prize_5d',
        'prize_4d',
        'prize_3d',
        'lucky_draw',
        'game_type_id'
	];
	
    public function gameType(){
        return $this->belongsTo(GameType::class);
    }

    protected $casts = [
        'result_number' => 'array',
    ];
	
}
