<?php

namespace App\Model\BALL;

use Illuminate\Database\Eloquent\Model;

class GameType extends Model
{
    protected $table = "ball_game_types";
    protected $fillable = [
        'id',
        'code',
        'name',	
        'min',
        'max',
        'start_prize',
        'win_prize_5d',
        'win_prize_4d',
        'win_prize_3d',
        'addon_percentage',
        'order_price',
        'lucky_draw'
    ];

}
