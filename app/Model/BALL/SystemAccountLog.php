<?php

namespace App\Model\BALL;

use Illuminate\Database\Eloquent\Model as Eloquent;

class SystemAccountLog extends Eloquent
{
	protected $table = "ball_system_account_logs";
    protected $fillable = [
        'id',
        'amount',
        'balance',
        'abstract',
        'user_abstract',
        'log_number',
        'log_type',
        'to_type',
        'account_id',
        'to_account_id',
        'manager_id'
    ];

    public function manager(){
        return $this->belongsTo(Manager::class,'manager_id','id');
    }

    public function account(){ 
        return $this->belongsTo(SystemAccount::class,'account_id','id');
    }

    public function toSystemAccount(){
        return $this->belongsTo(SystemAccount::class,'to_account_id','id');
    }
    public function toUser(){
        return $this->belongsTo(User::class,'to_account_id','id');
    }
    
    public static function generateLogNumber($logType){
        return $logType.addPrefixStringPad(auth()->id(),4,'0').date('Y').date('m').date('d').date('H').date('i').date('s');
    }
}
