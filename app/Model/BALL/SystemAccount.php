<?php

namespace App\Model\BALL;

use Illuminate\Database\Eloquent\Model as Eloquent;

class SystemAccount extends Eloquent
{
	protected $table = "ball_system_accounts";
    protected $fillable = [
        'id',
        'title',
        'number',
        'name',
        'balance',
        'sort',
        'state',
        'type_id',
        'category_id',
    ];

    public function type(){
        return $this->belongsTo(SystemAccountType::class);
    }
    public function category(){
        return $this->belongsTo(SystemAccountCategory::class);
    }
}
