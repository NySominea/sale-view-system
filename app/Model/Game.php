<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Game extends Eloquent
{
	protected $table = "d_games";

	protected $fillable = [
		'id',
		'name',
		'sort'
	];
}
