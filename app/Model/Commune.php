<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Commune extends Eloquent
{
	protected $fillable = [
        'id',
        'district_id',
        'code',
        'name'
    ];

    public function district(){
        return $this->belongsTo(District::class);
    }
}
