<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class GameCategory extends Eloquent
{
	protected $table = "d_game_categories";

    protected $fillable = [
            'id',
            'game_id',
            'name',
            'rebate',
            'rate',
            'sort'
    ];

    public function game(){
        return $this->belongsTo(Game::class,'game_id');
    }

    public function game_rebate(){
        return $this->hasOne(GameRebate::class,'category_id','id');
    }
}
