<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Manager extends Authenticatable
{
	use Notifiable;

    protected $fillable = [
        'id',
        'username', 
        'password',
        'pay_pass',
        'pass_salt',
        'nick_name',
        'is_super',
        'state',
        'last_login',
        'last_edit_password',
        'logout_count',
        'start_working_date',
        'parent_id',
        'employee_id',
        'lang_id',
        'role_id',
    ];

    public static function checkPayPassword($pwd){
        $manager = auth()->user();
        if(!$manager || $manager->password != md5($pwd.$manager->pass_salt) 
            || $manager->state !=1){
            return false;
		}
        return true;
    }

    public function managerLogs(){      
        return $this->hasMany(ManagerLog::class);       
    }
}
