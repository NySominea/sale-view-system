<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Language extends Eloquent
{
	protected $table = 'languages';
	
	protected $fillable = [
		'id',
        'language',
        'language_field',
        'sort'
	];
}
