<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\D5\Order;

class OrderPageColumn extends Model
{
    protected $guarded = [];

    function order(){
        return $this->belongsTo(Order::class,'order_id','id');
    }
}
