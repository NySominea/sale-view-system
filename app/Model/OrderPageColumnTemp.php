<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\D5\OrderTemp;

class OrderPageColumnTemp extends Model
{
    protected $guarded = [];

    function order(){
        return $this->belongsTo(OrderTemp::class,'order_id','id');
    }
}
