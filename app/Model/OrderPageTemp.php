<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\User;

class OrderPageTemp extends Model
{
    protected $guarded = [];

    function order_page_columns(){
        return $this->hasMany(OrderPageColumnTemp::class,'order_page_id','id');
    }

    function user(){
        return $this->belongsTo(User::class);
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d H:i A');
    }

}
