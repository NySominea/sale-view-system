<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Currency extends Eloquent
{
	protected $fillable = [
        'id',
        'name',
        'symbol',
        'unit',
    ];
}
