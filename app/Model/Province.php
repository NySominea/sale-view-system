<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Province extends Eloquent
{
	protected $fillable = [
        'id',
        'code',
        'name'
    ];

    public function districts(){
        return $this->hasMany(District::class);
    }
}
