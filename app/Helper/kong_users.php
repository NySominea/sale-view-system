<?php

function get_kong_users(){
    return [
        'kong',
        'kong1',
        'kong2',
        'kong3',
        'kong4',
        'kong5',
        'kong6',
        'kong7',
        'kong8',
        'kong9',
        'Test App',
        'Test ComDirect',
        'Test ComDirect1'
    ];
}

function get_kong_users_key(){
    return [
        'kong' => 'kong',
        'kong1' => 'kong1',
        'kong2' => 'kong2',
        'kong3' => 'kong3',
        'kong4' => 'kong4',
        'kong5' => 'kong5',
        'kong6' => 'kong6',
        'kong7' =>'kong7',
        'kong8' => 'kong8',
        'kong9' => 'kong9',
        'Test App' => 'Test App',
        'Test ComDirect' => 'Test ComDirect',
        'Test ComDirect1' => 'Test ComDirect1',
    ];
}

