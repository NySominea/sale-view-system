<?php
use App\CurrencyExchange;
use App\Model\Exchange;
use App\Model\Language;
use App\Model\LanguageAdmin;


function getUserLevelTitle($level){
    $title = 'COO';
    switch($level){
        case 1: $title = 'COO'; break;
        case 2: $title = 'Provincial Manager'; break;
        case 3: $title = 'District Sales'; break;
        case 4: $title = 'Network Master'; break;
        case 5: $title = 'Network Team Leader'; break;
        case 6: $title = 'Network Sales'; break;
        case 7: $title = 'Master Agent'; break;
        case 8: $title = 'Agent'; break;
        case 9: $title = 'This User'; break;
    }
    return $title;
}

function currencyFormat($amount,$decimal = 2){
    return number_format($amount, $decimal);
}
function stringToDouble($str){
    return doubleval(str_replace(',','',$str));
}
function exchangeRateFromRielToDollar(){
    $currency = Exchange::where('from_currency',1)->first();
    return $currency ? 1 / $currency->rate : 0;
}
function exchangeRateFromDollarToRiel(){
    $currency = Exchange::where('from_currency',1)->first();
    return $currency ? $currency->rate : 0;
}

function addPrefixStringPad($input,$length,$string){
    return str_pad($input, $length, $string, STR_PAD_LEFT);
}
function addSuffixStringPad($input,$length,$string){
    return str_pad($input, $length, $string, STR_PAD_RIGHT);
}

function getFrontendGeneralSetting(){ 
    $settings = [];
    $settings['exchangeRate'] = [
        'dollarToRiel' => exchangeRateFromDollarToRiel(),
        'rielToDollar' => exchangeRateFromRielToDollar(),
    ];
    $settings['language'] = getAdminLanguageData();
    $settings['avaiableLanguage'] = getAvailableLanguage();
    return $settings;
}

function getAvailableLanguage(){
    $language = Language::orderBy('sort','ASC')->get()->keyBy('language_field');
    return $language;
}
function getAdminLanguageData(){ 
    $currentLang = 'english';
    if(app()->getLocale() == 'en'){
        $currentLang = 'english';
    }elseif(app()->getLocale() == 'zh'){
        $currentLang = 'chinese';
    }elseif(app()->getLocale() == 'kh'){
        $currentLang = 'khmer';
    }  
    
    return LanguageAdmin::all()->pluck($currentLang,'label')->toArray();
}

function responseSuccess($data = [],$message = ''){
    return [
        'code' => 200,
        'data' => $data,
        'message' => $message
    ];
}

function responseError($message,$code = 403){
    return [
        'code' => $code,
        'data' => null,
        'message' => $message
    ];
}

function api_trans($msg,$key = []){
    return trans($msg,$key, getRequestLanguage());
}

function getRequestLanguage(){
    $lang = request()->header('Accept-Language') ?? 'en';
    return $lang;
}