<?php

namespace App\Http\Controllers\Admin\D5;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\D5\UserAccountLog;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logs = UserAccountLog::where('user_id',auth()->id())->paginate(15);
        return view('account_log',compact('logs'));
    }

}
