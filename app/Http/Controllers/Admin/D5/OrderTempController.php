<?php

namespace App\Http\Controllers\Admin\D5;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\D5\OrderTemp;
use App\Model\D5\Order;
use App\Model\D5\Cycle;
use App\User;
use DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class OrderTempController extends Controller
{
    private $authLevel; 
    private $authId;
    private $auth;

    public function index(Request $request)
    {   
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        $s = microtime(true);
        $showTicketDetail = false;
        $data = [];
        $breadcrumbs = "";
        $table = OrderTemp::getModel()->getTable();
        $s = microtime(true);
        if(!$request->user_id){
            $d = $this->structureSummaryData($table, $request);
            $total = isset($d['total']) ? $d['total'] : null; 
            $users = isset($d['data']) && count($d['data']) > 0 ? $d['data'] : null;
            $hasDirectUser = $d['hasDirectUser'];
            $parentHasOrder = $d['parentHasOrder'];
        }else{
            $d = $this->structureTicketDataForEachUser($table, $request);
            $total = isset($d['total']) ? $d['total'] : null;
            $users = isset($d['data'][0]) ? $d['data'][0] : null;
            $showTicketDetail = true;
            $hasDirectUser = $d['hasDirectUser'];
            $parentHasOrder = $d['parentHasOrder'];
        } 
        // dd(microtime(true) - $s);
        // Breadcrumbs
        $topLevelUsers = $this->getBreadcrumbList($request);
        
        return view('admin.D5.order.temp.index',compact('users','total','topLevelUsers','showTicketDetail','hasDirectUser','parentHasOrder'));
    }

    public function downloadExcel(){
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        $table = 'order_temps'; 
        $historyReport = false;
        $request = request();  
        $data = $this->structureTicketDataForEachUser($table,$request);
        $path = $this->writeExcel($data);
        return response()->json(['path' => $path, 'success' => true]);
    }

    public function writeExcel($data, $isHistoryList = false){
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Sale Report');
        $spreadsheet = $this->writeDetailWorksheet($spreadsheet, $sheet, $data['data'], $isHistoryList);
        
        $writer = new Xlsx($spreadsheet);
        if(!is_dir(public_path('backup'))) mkdir(public_path('backup'),777);
        $filename = "5D_Sale_Report(".date('Y-m-d').").xlsx";
        if(request()->cycle_id){
            $cycle = Cycle::whereId(request()->cycle_id)->first();
            if($cycle) $filename = "5D_Sale_Report(".$cycle->cycle_sn.").xlsx";
        }
        $writer->save(public_path('backup').'/'.$filename);
        $path = asset('backup/'.$filename);
        return $path;
    }

    public function writeDetailWorksheet($spreadsheet, $sheet, $data, $isHistoryList){
        
        $columnNames = $isHistoryList 
                        ? ['Account No','User','Ticket','Post Time','Prize', 'Bet','Type','Bet Amount','Ticket Amount','Win Amount']
                        : ['Account No','User','Ticket','Post Time', 'Prize', 'Bet','Type','Bet Amount','Ticket Amount'];
        
        $sheet = $this->writeHeaderWorksheet($sheet, $columnNames);
        $i = 2;
        foreach( $data as $l => $user){
            $sheet->setCellValue('A'.$i, $user['accountno']);
            $sheet->setCellValue('B'.$i, $user['username']);
            $j = 0; 
            foreach($user['ticket'] as $key => $ticket){
                $totalTicket = 0;
                $sheet->setCellValueExplicit('C'.($i + $j), $ticket['ticket'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $sheet->setCellValue('D'.($i + $j), $ticket['created_at']);
                $sheet->setCellValue('I'.($i + $j), preg_replace("/([^0-9\\.])/i", "", $ticket['ticketAmountR']));
                if($isHistoryList)
                    $sheet->setCellValue('J'.($i + $j), preg_replace("/([^0-9\\.])/i", "", $ticket['ticketWinAmountR']));
                foreach($ticket['orders'] as $index => $o){
                    $sheet->setCellValueExplicit('E'.($i + $j + $index), $o['prize'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $sheet->setCellValueExplicit('F'.($i + $j + $index), $o['bet'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $sheet->setCellValue('G'.($i + $j + $index), $o['type']);
                    $sheet->setCellValue('H'.($i + $j + $index), $o['amountR']);  
                    $totalTicket += (double)$o['amountR'];
                }
                if((double)$ticket['ticketAmountR'] != $totalTicket){
                    $sheet->setCellValue('E'.($i + $j + $index + 1), 'Lost Number');
                    $sheet->setCellValue('F'.($i + $j + $index + 1), 'Lost Number');
                    $sheet->setCellValue('H'.($i + $j + $index + 1), (double)$ticket['ticketAmountR']-$totalTicket); 
                    $j+=1;
                }
                $j+=count($ticket['orders']);
            }
            $i+=$j;
        }
        
        return $spreadsheet;
    }

    public function writeSummaryWorksheet($spreadsheet, $sheet, $data, $isHistoryList){
        
        $columnNames = ['User','Account No','Total Bet Amount','Total Win Amount'];
        
        $sheet = $this->writeHeaderWorksheet($sheet, $columnNames);
        $i = 2;
        foreach( $data as $user){
            $sheet->setCellValue('A'.$i, $user['username']);
            $sheet->setCellValue('B'.$i, $user['accountno']);
            $sheet->setCellValue('C'.$i, $user['totalAmount']);
            $sheet->setCellValue('D'.$i, $user['totalWinAmount']);
            $i++;
        }

        return $spreadsheet;
    }

    public function writeHeaderWorksheet($sheet, $columnNames){
        $columns = [];
        $a = 'A';
        foreach($columnNames as $index => $value){
            array_push($columns, $a++);
        }

        for($iterator=0;$iterator<count($columns);$iterator++){
            $sheet->setCellValue($columns[$iterator].'1', $columnNames[$iterator]);
            $sheet->getStyle($columns[$iterator].'1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('888888');

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->getColor()
                ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setSize(11);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);
            $sheet->getColumnDimension($columns[$iterator])->setAutoSize(true);
        }

        return $sheet;
    }

    public function getBreadcrumbList($request){
        $arrayId = [];
        $topLevelUsers = [];
        $thisUser = $request->parent_id == 0 ? User::find($request->user_id) : User::find($request->parent_id);
        if($thisUser){
            for($i = 1; $i < $thisUser->level; $i++){
                $arrayId[] = $thisUser->{'l'.$i.'_id'};
            }
            $topLevelUsers = User::whereIn('id', $arrayId)->orderBy('level','ASC')->get()->toArray();
            $topLevelUsers[] = $thisUser->toArray();
        }
        return $topLevelUsers;
    }

    public function structureSummaryData($table, $request){ $s = microtime(true);
        $data = [];
        $total = [];
        $lv = 1;
        $p = $request->parent_id ?? null;
        $arrUserIds = [];
        $exchangeRate = exchangeRateFromRielToDollar(); 
        $hasDirectUser = false;
        $parentHasOrder = false;
        $auth = auth()->user(); 
        $this->authLevel = $auth->level;
        $this->authId = $auth->id;
        if($request->level < $this->authLevel) abort(403);
        

        // Get List of users for current orders
        $userQuery = ['level' => $lv];
        if($request->level && $request->parent_id){
            $lv = $request->level == 8 ? $request->level : $request->level + 1;
            $p = $request->parent_id;
            $userQuery = ['level' => $lv, 'parent_id' => $p];
        }
        $users = User::select('id','level','username','account_number')
                    ->where($userQuery)
                    ->orWhere(function($q) use ($p){
                        $q->where(['id' => $p]);
                    })
                    ->orWhere(function($q) use ($request){
                        $q->where(['parent_id' => 0, 'com_direct' => 1]);
                    })
                    ->get();
        
        if($users->count() <= 0){
            $lv = $request->level;
            $users = User::where(['level' => $lv, 'id' => $p])->get();
        } 
        // End Get List of users
        // dd(microtime(true) - $s);
        // Query on conditional filter
        $arrUserIds = $users->pluck('id')->toArray();
        $query = $table == 'd_orders' ? Order::select('ticket','user_id','win_amount','amount','l'.$lv.'_id','l'.($request->level == 8 ? $request->level : $request->level+1).'_id','l'.$lv.'_rebate', 'user_type','parent_id','state','cycle_id','created_at')
                                      : OrderTemp::select('ticket','user_id','win_amount','amount','l'.$lv.'_id','l'.($request->level == 8 ? $request->level : $request->level+1).'_id','l'.$lv.'_rebate', 'user_type','parent_id','state','cycle_id','created_at');
       
        $query->when($request->cycle_id, function($q) use ($request){
            return $q->where(['cycle_id' => $request->cycle_id]);
        }); 
        $query->when($request->begin_date && $request->end_date, function($q) use ($request){ 
            return $q->whereBetween('created_at',[$request->begin_date, $request->end_date]);
        });
        
        $query->when($this->authLevel && $this->authId, function($q){
            return $q->where(['l'.$this->authLevel.'_id' => $this->authId]);
        });
        
        if(!$request->level && !$request->parent_id) 
            array_push($arrUserIds,0); // Get records of direct users
        
        $topUsers = $query->where('state',1)->whereIn('l'.$lv.'_id',$arrUserIds)->orderBy('user_type','DESC')->orderBy('l'.$lv.'_id','ASC')
                        ->orWhere(function($q) use ($p, $request){
                            $q->where(['user_id' => $p])
                            ->when($request->cycle_id,function($qq) use ($request){
                                $qq->where('cycle_id',$request->cycle_id);
                            })
                            ->when($request->begin_date && $request->end_date, function($qq) use ($request){
                                $qq->whereBetween('created_at',[$request->begin_date, $request->end_date]);
                            });
                        })
                        ->get()
                        ->groupBy('l'.$lv.'_id','user_id');
                  
        // End query on conditional filter
        
        $totalOrderAmount = 0;
        $totalMemberAmount = 0; 
        $totalAmount = 0;
        $totalRebate = 0;
        $totalWinAmount = 0;
        $totalProfitAmount = 0;
        $usersKeyById = $users->keyBy('id');
        
        foreach($topUsers as $id => $user){
            $children = $user->groupBy('l'.($request->level+1).'_id');
            $agents = $user->groupBy('user_id');
            $memberAmount = $agents->count();
            $orderAmount = 0;
            $amount = 0;
            $rebate = 0;
            $winAmount = 0;
            $userType = 2;
            $hasChildren = false;
            if($id == 0){
                $hasChildren = false;
                foreach($agents as $i => $agent){ 
                    if($agent->first()->user_type == 2){
                        $parentHasOrder = true;
                        $hasDirectUser = false;
                    }else{
                        $parentHasOrder = false;
                        $hasDirectUser = true;
                    }
                    $memberAmount = 1;
                    $orderAmount = 0;
                    $amount = 0;
                    $rebate = 0;
                    $winAmount = 0;
                    $orderAmount += $agent->groupBy('ticket')->count();
                    foreach($agent as $order){
                        $amount += $order->amount;
                        $rebate += $order->{'l'.$lv.'_rebate'};
                        $winAmount += $order->win_amount;
                    }
                    $data[] = [
                        'user'      => $usersKeyById[$agent->first()->user_id]->toArray(),
                        'memberAmount'   => $memberAmount,
                        'orderAmount'    => currencyFormat($orderAmount,0),
                        'amountR'    => currencyFormat($amount),
                        'amountD'    => currencyFormat($amount * $exchangeRate),
                        'rebateR' => currencyFormat($rebate),   
                        'rebateD' => currencyFormat($rebate * $exchangeRate),
                        'winAmountR'=> currencyFormat($winAmount),
                        'winAmountD'=> currencyFormat($winAmount * $exchangeRate),
                        'profitAmountR' => currencyFormat($amount - $winAmount - $rebate),
                        'profitAmountD' => currencyFormat(($amount - $winAmount - $rebate)  * $exchangeRate),
                        'userType' => $agent->first()->user_type,
                        'hasChildren' => $hasChildren,
                    ]; 
                    
                    $totalOrderAmount += $orderAmount;
                    $totalMemberAmount += $memberAmount;
                    $totalAmount += $amount;
                    $totalRebate += $rebate;
                    $totalWinAmount += $winAmount;
                    $totalProfitAmount += $amount - $winAmount - $rebate;
                }
            }else{
                foreach($agents as $agent){
                    $orderAmount += $agent->groupBy('ticket')->count();
                    foreach($agent as $order){
                        $amount += $order->amount;
                        $rebate += $order->{'l'.$lv.'_rebate'};
                        $winAmount += $order->win_amount;
                        if($hasDirectUser){
                            $userType = 1;
                            $hasChildren = false;
                        }
                    }
                }
                $hasChildren = isset($children[$id]) ? $children[$id]->count() > 0 : false;
                $data[] = [
                    'user'      => $usersKeyById[$hasDirectUser ? $user->first()->user_id : $id]->toArray(),
                    'memberAmount'   => $memberAmount,
                    'orderAmount'    => currencyFormat($orderAmount,0),
                    'amountR'    => currencyFormat($amount),
                    'amountD'    => currencyFormat($amount * $exchangeRate),
                    'rebateR' => currencyFormat($rebate),
                    'rebateD' => currencyFormat($rebate * $exchangeRate),
                    'winAmountR'=> currencyFormat($winAmount),
                    'winAmountD'=> currencyFormat($winAmount * $exchangeRate),
                    'profitAmountR' => currencyFormat($amount - $winAmount - $rebate),
                    'profitAmountD' => currencyFormat(($amount - $winAmount - $rebate)  * $exchangeRate),
                    'userType' => $userType,
                    'hasChildren' => $hasChildren,
                ];

                $totalOrderAmount += $orderAmount;
                $totalMemberAmount += $memberAmount;
                $totalAmount += $amount;
                $totalRebate += $rebate;
                $totalWinAmount += $winAmount;
                $totalProfitAmount += $amount - $winAmount - $rebate;
            }
        }
        $total = [
            'memberAmount' => currencyFormat($totalMemberAmount,0),
            'orderAmount' => currencyFormat($totalOrderAmount,0),
            'amountR' => currencyFormat($totalAmount),
            'amountD' => currencyFormat($totalAmount * $exchangeRate),
            'rebateR' => currencyFormat($totalRebate),
            'rebateD' => currencyFormat($totalRebate * $exchangeRate),
            'winAmountR' => currencyFormat($totalWinAmount),
            'winAmountD' => currencyFormat($totalWinAmount * $exchangeRate),
            'profitAmountR' => currencyFormat($totalProfitAmount),
            'profitAmountD' => currencyFormat($totalProfitAmount * $exchangeRate)
        ];
        
        return ['data' => $data, 'total' => $total, 'hasDirectUser' => $hasDirectUser, 'parentHasOrder' => $parentHasOrder];
    }

    public function structureTicketDataForEachUser($table, $request){
        $data = [];
        $totalAmount = 0;
        $totalWinAmount = 0;
        $user_id = $request->user_id;
        $exchangeRate = exchangeRateFromRielToDollar();
        $auth = auth()->user();
        $this->authLevel = $auth->level;
        $this->authId = $auth->id;
        if($request->level < $this->authLevel) abort(403);
        $query = $table == 'd_orders' ? Order::select()
                                    : OrderTemp::select();
        $ordersByUser = $query
                            ->when($request->level && $request->parent_id && $request->cycle_id,function($q) use ($request){
                                return $q->where(['l'.$request->level.'_id' => $request->parent_id, 'cycle_id' => $request->cycle_id]);
                            })
                            ->when($user_id && $request->cycle_id,function($q) use ($user_id,$request){
                                return $q->where(['user_id' => $user_id, 'cycle_id' => $request->cycle_id]);
                            })
                            ->when($request->parent_id && $request->level,function($q) use ($request){
                                return $q->where(['l'.$request->level.'_id' => $request->parent_id]);
                            })
                            ->when($request->cycle_id,function($q) use ($request){
                                return $q->where('cycle_id',$request->cycle_id);
                            })
                            ->when($request->level && $request->parent_id && $request->begin_date && $request->end_date,function($q) use ($request){
                                return $q->where(['l'.$request->level.'_id' => $request->parent_id])->whereBetween('created_at',[$request->begin_date, $request->end_date]);
                            })
                            ->when($user_id && $request->begin_date && $request->end_date,function($q) use ($user_id,$request){
                                return $q->where(['user_id' => $user_id])->whereBetween('created_at',[$request->begin_date, $request->end_date]);
                            })
                            ->when($request->begin_date && $request->end_date,function($q) use ($request){
                                return $q->whereBetween('created_at',[$request->begin_date, $request->end_date]);
                            })
                            ->when($request->user_type, function($q) use ($request){ 
                                return $q->where('user_type',$request->user_type);
                            })
                            ->when($this->authLevel && $this->authId, function($q){
                                return $q->where(['l'.$this->authLevel.'_id' => $this->authId]);
                            })
                            ->with('user','cycle')
                            ->orderBy('created_at')
                            ->get(); 
                
        $totalWinAmount = 0;
        $totalAmount = 0;
        foreach($ordersByUser->groupBy('user_id') as $userOrders){ 
            $t = [];
            foreach($userOrders as $no => $order){
                $o = [];
                $bets = $order->bet_content;
                
                if(isset($bets)){
                    foreach($bets as $bet){
                        $o[] = [
                            'amountR' => $bet[2],
                            'amountD' => $bet[2] * $exchangeRate,
                            'winAmountR' => 0,
                            'winAmountD' => 0 * $exchangeRate,
                            'prize' => $bet[0],
                            'bet' => $bet[1],
                            'unit' => substr_replace($bet[3],"",-1).'00',
                            'type' => $bet[4]
                        ];
                    }
                    $t[] = [
                        'cycle'     => $order->cycle->cycle_sn,
                        'ticket'    => $order->ticket,
                        'created_at'=> $order->created_at,
                        'ticketAmountR' => $order->amount,
                        'ticketAmountD' => $order->amount * $exchangeRate,
                        'ticketWinAmountR'=> $order->win_amount,
                        'ticketWinAmountD'=> $order->win_amount * $exchangeRate,
                        'orders'    => $o
                    ];
                }
                
            }
            $data[] = [
                'username'  => $userOrders[0]->user->username,
                'accountno' => $userOrders[0]->user->account_number,
                'totalAmountR' => $userOrders->sum('amount'),
                'totalAmountD' => $userOrders->sum('amount') * $exchangeRate,
                'totalWinAmountR' => $userOrders->sum('win_amount'),
                'totalWinAmountD' => $userOrders->sum('win_amount') * $exchangeRate,
                'orders_no' => $userOrders->count(),
                'ticket'    => $t,
            ]; 
            $totalWinAmount += $userOrders->sum('win_amount');
            $totalAmount += $userOrders->sum('amount');
        }   
        $total = [
            'winAmountR' => $totalWinAmount,
            'winAmountD' => $totalWinAmount * $exchangeRate,
            'amountR' => $totalAmount,
            'amountD' => $totalAmount * $exchangeRate
        ];
        
        return ['data' => $data, 'total' => $total, 'hasDirectUser' => false,'parentHasOrder' => false];
    }
}
