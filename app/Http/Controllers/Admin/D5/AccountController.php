<?php

namespace App\Http\Controllers\Admin\D5;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use DB;

class AccountController extends Controller
{
    public function index($id)
    {
        DB::beginTransaction();
        try{
            $user = User::with('userAccounts')->whereId($id)->first();
            $accounts = $user->userAccounts;
            if(!$user)
                return redirect()->back()->withInput()->withError('Unknown User!');
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withInput()->withError('There was an error during operation!');
        }
        return view('admin.D5.user.account',compact('user','accounts'));
    }
}
