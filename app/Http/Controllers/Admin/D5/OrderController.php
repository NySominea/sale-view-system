<?php

namespace App\Http\Controllers\Admin\D5;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\D5\Cycle;
use App\Model\D5\OrderTemp;
use App\Model\D5\Order;
use App\User;
use DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Http\Controllers\Admin\D5\OrderTempController;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        $s = microtime(true);
        $showTicketDetail = false;
        $data = [];
        $breadcrumbs = "";
        $orderTempController = new OrderTempController;
        $cycle = $request->cycle_id ? Cycle::findOrFail($request->cycle_id) : null;
        $table = Order::getModel()->getTable();
        if(!$request->user_id){
            $d = $orderTempController->structureSummaryData($table, $request);
            $total = isset($d['total']) ? $d['total'] : null; 
            $users = isset($d['data']) && count($d['data']) > 0 ? $d['data'] : null;
            $hasDirectUser = $d['hasDirectUser'];
            $parentHasOrder = $d['parentHasOrder'];
        }else{
            $d = $orderTempController->structureTicketDataForEachUser($table, $request);
            $total = isset($d['total']) ? $d['total'] : null;
            $users = isset($d['data'][0]) ? $d['data'][0] : null;
            $showTicketDetail = true;
            $hasDirectUser = $d['hasDirectUser'];
            $parentHasOrder = $d['parentHasOrder'];
        }
        // Breadcrumbs
        $topLevelUsers = $orderTempController->getBreadcrumbList($request);
        
        return view('admin.D5.order.report.index',compact('cycle','users','total','topLevelUsers','showTicketDetail','hasDirectUser','parentHasOrder'));
    }

    public function downloadExcel(){
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');

        $orderTempController = new OrderTempController;
        $table = 'd_orders'; 
        $request = request();
        $data = $orderTempController->structureTicketDataForEachUser($table,$request);
        $path = $orderTempController->writeExcel($data,true);
        return response()->json(['path' => $path, 'success' => true]);
    }

    public function showSelectionForm(){
        $cycles = Cycle::orderBy('cycle_sn','DESC')->where(['state' => 0, 'has_released' => 1])->pluck('cycle_sn','id')->toArray();
        $cycles = ['0' => 'Choose'] + $cycles;
        return view('admin.D5.order.report.selection-form',compact('cycles'));
    }
}
