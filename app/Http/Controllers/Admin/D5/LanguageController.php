<?php

namespace App\Http\Controllers\Admin\D5;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class LanguageController extends Controller
{
    public function setLocale($locale){
        Session::put('locale',$locale);
        return redirect()->back();
    }
}
