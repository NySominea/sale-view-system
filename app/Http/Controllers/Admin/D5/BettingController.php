<?php

namespace App\Http\Controllers\Admin\D5;

use App\Http\Services\OrderService;
use App\Http\Controllers\Controller;
use Exception;
use App\Model\D5\Cycle;
use App\Model\D5\Order;
use App\Model\D5\OrderTemp;
use App\Model\OrderPageTemp;
use App\Model\OrderPage;
use App\Model\OrderPageColumn;
use App\Model\OrderPageColumnTemp;
use App\User;
use DB;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class BettingController extends Controller
{
    public function index(){
        $balance = auth()->user()->userCashAccount->balance;
        $activeCycle = Cycle::whereHasReleased(0)
                        ->whereState(1)
                        ->where('stopped_time','>', time())->first();

        return view('admin.D5.betting.bet', compact('activeCycle','balance'));
    }

    public function getRecord(){
        $recordCycles = $this->record_cycles();
        // dd($recordCycles);
        return view('admin.D5.record.record', compact('recordCycles'));
    }

    public function downloadRecordCycle(Cycle $cycle){
        if(!$cycle->has_released)
            $sheets = OrderPageTemp::with('order_page_columns.order','user');
        else
            $sheets = OrderPage::with('order_page_columns.order','user')
                        ->where('cycle_id',$cycle->id);

        $spreadsheet = new Spreadsheet();

        $columnNames = [
            'Page No', 'Name', '2D', '3D', '5D',' Win 2D','Win 3D','Win 5D','Total','Total Win','%'
        ];

        $excelSheet = $spreadsheet->getActiveSheet();

        $current_column = 65;

        //render column
        for($iterator=0;$iterator < count($columnNames);$iterator++){
            $char_column = chr($current_column);
            $excelSheet->setCellValue($char_column.'1', $columnNames[$iterator]);
            $excelSheet->getStyle($char_column.'1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('888888');

            $excelSheet->getStyle($char_column.'1')
                ->getFont()
                ->getColor()
                ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

            $excelSheet->getStyle($char_column.'1')
                ->getFont()
                ->setSize(11);

            $excelSheet->getStyle($char_column.'1')
                ->getFont()
                ->setBold(true);
            $excelSheet->getColumnDimension($char_column)->setAutoSize(true);

            $current_column++;
        }
        // foreach($columnNames as $key => $name){
        //     $excelSheet->setCellValue(chr($current_column).'1', $name);
        //     $current_column++;
        // }

        $allUsers = User::whereIn('username',get_kong_users())->get();

        $currentRow = 2;

        foreach($allUsers as $user){
            $data = $sheets->where('user_id',$user->id)->get();

            $count2d = 0;
            $count3d = 0;
            $count5d = 0;

            $countWin2d = 0;
            $countWin3d = 0;
            $countWin5d = 0;

            if($data->isNotEmpty()){
                foreach($data as $order_page){
                    $orderPageColumns = $order_page->order_page_columns;
                    $count2d += $orderPageColumns->sum('total_amount_2d');
                    $count3d += $orderPageColumns->sum('total_amount_3d');
                    $count5d += $orderPageColumns->sum('total_amount_5d');

                    $countWin2d += $orderPageColumns->sum('total_win_2d');
                    $countWin3d += $orderPageColumns->sum('total_win_3d');
                    $countWin5d += $orderPageColumns->sum('total_win_5d');
        
                    $excelSheet->setCellValue('A'.$currentRow, $order_page->page);
                    $excelSheet->setCellValue('B'.$currentRow, $user->username);
                    $excelSheet->setCellValue('C'.$currentRow, $orderPageColumns->sum('total_amount_2d'));
                    $excelSheet->setCellValue('D'.$currentRow, $orderPageColumns->sum('total_amount_3d'));
                    $excelSheet->setCellValue('E'.$currentRow, $orderPageColumns->sum('total_amount_5d'));
                    $excelSheet->setCellValue('F'.$currentRow, $orderPageColumns->sum('total_win_2d'));
                    $excelSheet->setCellValue('G'.$currentRow, $orderPageColumns->sum('total_win_3d'));
                    $excelSheet->setCellValue('H'.$currentRow, $orderPageColumns->sum('total_win_5d'));
                    $excelSheet->setCellValue('I'.$currentRow, 
                        $orderPageColumns->sum('total_amount_2d') + 
                        $orderPageColumns->sum('total_amount_3d') + 
                        $orderPageColumns->sum('total_amount_5d')
                    );
                    $excelSheet->setCellValue('J'.$currentRow, 
                        $orderPageColumns->sum('total_win_2d') + 
                        $orderPageColumns->sum('total_win_3d') + 
                        $orderPageColumns->sum('total_win_5d')
                    );

                    $currentRow++;
                }

                $excelSheet->setCellValue('A'.$currentRow, 'Total');
                
                $excelSheet->setCellValue('C'.$currentRow, $count2d);
                $excelSheet->setCellValue('D'.$currentRow, $count3d);
                $excelSheet->setCellValue('E'.$currentRow, $count5d);
                $excelSheet->setCellValue('F'.$currentRow, $countWin2d);
                $excelSheet->setCellValue('G'.$currentRow, $countWin3d);
                $excelSheet->setCellValue('H'.$currentRow, $countWin5d);
                $excelSheet->setCellValue('I'.$currentRow, 
                    $count2d + $count3d + $count5d
                );
                $excelSheet->setCellValue('J'.$currentRow, 
                    $countWin2d + $countWin3d + $countWin5d
                );

                $currentRow ++;
            }
        }


        $writer = new Xlsx($spreadsheet);
        $writer->save('cycle_'.date('Y_m_d_H:i',$cycle->result_time).'.xlsx');
        
        return response()->download('cycle_'.date('Y_m_d_H:i',$cycle->result_time).'.xlsx');
        
    }

    // public function downloadRecordCycle(Cycle $cycle){
        
    //     if(!$cycle->has_released)
    //         $sheets = OrderPageTemp::whereUserId(auth()->id())
    //                     ->with('order_page_columns.order','user')->get();
    //     else
    //         $sheets = OrderPage::whereUserId(auth()->id())
    //                     ->with('order_page_columns.order','user')
    //                     ->where('cycle_id',$cycle->id)
    //                     ->get();

    //     $spreadsheet = new Spreadsheet();

    //     $table_head = [
    //         'borders' => [
    //             'right' => [
    //                 'color' => [
    //                     'rgb' => 'ffffff'
    //                 ],
    //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
    //             ],
    //         ],
    //         'font' => [
    //             'size' => 10,
    //             'color' => [
    //                 'rgb' => 'ffffff'
    //             ]
    //             ],
    //         'fill' => [
    //             'fillType' => Fill::FILL_SOLID,
    //             'startColor' => [
    //                 'rgb' => '538ed5'
    //             ]
    //         ]
    //     ];

    //     $even_row = [
    //         'borders' => [
    //             'right' => [
    //                 'color' => [
    //                     'rgb' => 'ffffff'
    //                 ],
    //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
    //             ],
    //         ],
    //         'font' => [
    //             'size' => 10,
    //             'color' => [
    //                 'rgb' => 'ffffff'
    //             ]
    //             ],
    //         'fill' => [
    //             'fillType' => Fill::FILL_SOLID,
    //             'startColor' => [
    //                 'rgb' => '1CC6FF'
    //             ]
    //         ]
    //     ];

    //     $odd_row = [
    //         'borders' => [
    //             'right' => [
    //                 'color' => [
    //                     'rgb' => 'ffffff'
    //                 ],
    //                 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
    //             ],
    //         ],
    //         'font' => [
    //             'size' => 10,
    //             'color' => [
    //                 'rgb' => 'ffffff'
    //             ]
    //             ],
    //         'fill' => [
    //             'fillType' => Fill::FILL_SOLID,
    //             'startColor' => [
    //                 'rgb' => '4379FF'
    //             ]
    //         ]
    //     ];

    //     $spreadsheet->getDefaultStyle()
    //         ->getFont()->setName('Arial');

    //     $spreadsheet->removeSheetByIndex(0);

        

    //     // dd($sheets);
    //     foreach($sheets as $index => $sheet){
    //         $current_column = 65;
    //         $excelSheet = new Worksheet($spreadsheet, 'Sheet '.$index);
    //         $excelSheet->setShowGridLines(false);
    //         $spreadsheet->addSheet($excelSheet);

    //         // $excelSheet->getStyle("A1")->getFont()->setSize(16);

    //         $excelSheet->setCellValue('A1', 'Date: '.$sheet->created_at);
    //         $excelSheet->setCellValue('A2', 'Result Time: '.date('Y-m-d H:i A',$cycle->result_time));
    //         $excelSheet->setCellValue('A3', 'User: '.$sheet->user->username);


    //         $excelSheet->setCellValue('E1', 'Total Win: '.$sheet->order_page_columns->sum('total_win_amount').' (R)');
    //         $excelSheet->setCellValue('E2', 'Total Win 2D: '.$sheet->order_page_columns->sum('total_win_2d'). ' (R)');
    //         $excelSheet->setCellValue('E3', 'Total Win 3D: '.$sheet->order_page_columns->sum('total_win_3d'). ' (R)');
    //         $excelSheet->setCellValue('E4', 'Total Win 5D: '.$sheet->order_page_columns->sum('total_win_5d').' (R)');

    //         $excelSheet->setCellValue('C1', 'Total: '.$sheet->order_page_columns->sum('total_amount').' (R)');
    //         $excelSheet->setCellValue('C2', 'Total 2D: '.$sheet->order_page_columns->sum('total_amount_2d'). ' (R)');
    //         $excelSheet->setCellValue('C3', 'Total 3D: '.$sheet->order_page_columns->sum('total_amount_3d'). ' (R)');
    //         $excelSheet->setCellValue('C4', 'Total 5D: '.$sheet->order_page_columns->sum('total_amount_5d').' (R)');

    //         $count = 1;
    //         foreach($sheet->order_page_columns as $order_page_column){
    //             // dd($sheet->order_page_columns);
    //             $start_row = 6;
    //             $current_column_char = chr($current_column);
                
    //             $header_cell = $current_column_char.$start_row;

    //             //setup header
    //             $excelSheet->getColumnDimension($current_column_char)->setWidth(30);
    //             $excelSheet->getStyle($header_cell)->getAlignment()->setWrapText(true);
    //             $excelSheet->getStyle($header_cell)->applyFromArray($table_head);
    //             $excelSheet->getRowDimension($header_cell)->setRowHeight(-1);
    //             $excelSheet->setCellValue($current_column_char.$start_row,
    //                 "Column $count
    //                 \nAmount 2D: $order_page_column->total_amount_2d (R)
    //                 \nAmount 3D: $order_page_column->total_amount_3d (R)
    //                 \nAmount 5D: $order_page_column->total_amount_5d (R)
    //                 \nWin Amount 2D: $order_page_column->total_win_amount_2d (R)
    //                 \nWin Amount 3D: $order_page_column->total_win_amount_3d (R)
    //                 \nWin Amount 5D: $order_page_column->total_win_amount_5d (R)
    //             ");

    //             $start_row = 7;
    //             $bet_contents = $order_page_column->order->bet_content;

    //             $count_bet = 0;
    //             foreach($bet_contents as $bet_content){
    //                 $excelSheet->getStyle($current_column_char.$start_row)
    //                     ->applyFromArray($count_bet % 2 == 0 ? $even_row : $odd_row);

    //                 $excelSheet->getStyle($current_column_char.$start_row)->getAlignment()
    //                     ->setHorizontal(Alignment::HORIZONTAL_LEFT);
    //                 $excelSheet->setCellValue($current_column_char.$start_row, 
    //                     $bet_content[1].' = '.$bet_content[2].' ('.$bet_content[0].') (Win = '.$bet_content[5].')'
    //                 );
    //                 $start_row++;
    //                 $count_bet++;
    //             }

    //             $current_column++;
    //             $count++;
    //         }

    //     }

    //     $writer = new Xlsx($spreadsheet);
    //     $writer->save('data.xlsx');
        
    //     return response()->download('data.xlsx');
    // }

    public function getRecordByCycle(Cycle $cycle){

        if(!$cycle->has_released)
            $sheets = OrderPageTemp::whereUserId(auth()->id())
                        ->with('order_page_columns','user')->get();
        else
            $sheets = OrderPage::whereUserId(auth()->id())
                        ->with('order_page_columns','user')
                        ->where('cycle_id',$cycle->id)
                        ->get();

    
        $win_info = [];

        foreach($sheets as $sheet){
            $win_info[$sheet->id] = [
                'total_win_2d' => $sheet->order_page_columns->sum('total_win_2d'),
                'total_win_3d' => $sheet->order_page_columns->sum('total_win_3d'),
                'total_win_5d' => $sheet->order_page_columns->sum('total_win_5d'),
                'total_win_amount' => $sheet->order_page_columns->sum('total_win_amount')
            ];
        }
    
        return view('admin.D5.record.detail',compact('sheets','cycle','win_info'));
    }

    private function record_cycles(){
        $current_cycle = [];
        $user_id = auth()->id();

        $cycles = Cycle::select('id','result_time')
                        ->whereHasReleased(1)
                        ->whereHas('orders',function($query) use ($user_id){
                            $query->where('user_id',$user_id);
                        })
                        ->distinct('id')->orderBy('id','desc')->limit(100)->get();

        $cycles = $cycles->keyBy(function ($value, $key) {
            if ($key == 'data') {
                return 'cycles';
            } else {
                return $key;
            }
        })->values()->all();

        $current_cycle = Cycle::select('id','result_time')->whereHasReleased(0)->whereHas('order_temps',function($query) use ($user_id){
            $query->where('user_id',$user_id);
        })->get();

        $current_cycle = $current_cycle ? $current_cycle->toArray() : [];

        $cycles = array_merge($current_cycle,$cycles);

        return $cycles;
    }

    function ticket_info(){
        $cycle_id = request()->get('cycle_id');
        $is_normal_mode = request()->get('is_normal_mode',1) == 1? 'win_amount >= 0' : 'win_amount > 0';
        $user_id = auth()->id();
        $order_table = Order::getModel()->getTable();
        $user_table = User::getModel()->getTable();
        $cycle_table = Cycle::getModel()->getTable();

        $cycle = Cycle::find($cycle_id);
        if(!$cycle->has_released){
            $order_table = OrderTemp::getModel()->getTable();
        }

        $orders = DB::select(
            DB::raw("SELECT
            ticket,
            DATE_FORMAT($order_table.created_at, '%Y-%m-%d %h:%i %p') as created_at,
            amount,
            result_time,
            win_amount,
            bet,
            bet_content,
            username,
            account_number,
            is_settle
            FROM $order_table,$cycle_table,$user_table WHERE
            $order_table.cycle_id = $cycle_table.id
            AND $order_table.user_id = $user_table.id
            AND user_id = $user_id
            AND $order_table.cycle_id = $cycle->id
            AND $is_normal_mode
            ORDER BY $order_table.created_at desc"));


        foreach($orders as $order){
            $order->cycle = $cycle;
        }

        $total = DB::select(
            DB::raw("SELECT
                SUM(amount) as total_amount,
                SUM(win_amount) as total_win_amount,
                SUM(win_2d_amount) as total_2d_amount,
                SUM(win_3d_amount) as total_3d_amount,
                SUM(win_5d_amount) as total_5d_amount
                FROM $order_table WHERE cycle_id = $cycle->id
            ")
        )[0];

        return response()->json([
            'orders' => $orders,
            'total' => $total
        ]);
    }

    public function order(){
        $this->validate(request(),[
            'bet_body' => 'required'
        ]);

        $bet_body = request()->get('bet_body');
        $bet_body = array_filter($bet_body);

        DB::beginTransaction();
        try{
            $orders = [];

            $cycle = Cycle::whereHasReleased(0)
                    ->orderBy('id','desc')
                    ->first();

            if(!$cycle) throw new Exception(api_trans('cycle.no_cycle'));

            if(time() > $cycle->stopped_time) throw new Exception(api_trans('cycle.cycle_stop'));

            $count_order_page = OrderPageTemp::whereUserId(auth()->id())->count();

            $order_page = OrderPageTemp::create([
                'user_id' => auth()->id(),
                'cycle_id' => $cycle->id,
                'page' => $count_order_page + 1,
            ]);


            foreach($bet_body as $column => $val){
                if($val){
                    $orderService = new OrderService($val,null,null);
                    $response = $orderService->process_bet($column, $order_page->id);

                    if(!$response) throw Exception('Something went wrong!');

                    $orders[] = $response;
                }
            }

            if(count($orders) == count($bet_body)){
                $orders = collect($orders);

                $order_page->total_amount_2d = $orders->sum('total_amount_2d');
                $order_page->total_amount_3d = $orders->sum('total_amount_3d');
                $order_page->total_amount_5d = $orders->sum('total_amount_5d');

                $balance = auth()->user()->userCashAccount->balance;

                $order_page->save();
                DB::commit();
                return responseSuccess([
                    'msg' => 'Submitted!',
                    'balance' => number_format($balance)
                ]);
            }else{
                throw Exception('Something went wrong! (x2)');
            }

           
            
        }catch(Exception $ex){
            DB::rollback();

            return responseError($ex->getMessage());
        }
    }
}
