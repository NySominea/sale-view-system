<?php

namespace App\Http\Controllers\Admin\D5;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\OrderPage;
use DB;
use App\User;
use App\Model\D5\Cycle;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ReportController extends Controller
{
    function showReportForm(){
        return view('admin.D5.report.report-form');
    }

    function downloadReportToday(){
        $time = strtotime("today 00:00:00");

        $cycles = Cycle::where('result_time','>',$time)->where('has_released',1)->get();


        $columnNames = [
            'Date', 'Time','ID', 'Page', 'Total Sale','Total Win 2D','Total Win 3D','Total Win'
        ];

        $current_column = 65;
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        //render column
        for($iterator=0;$iterator < count($columnNames);$iterator++){
            $char_column = chr($current_column);
            $sheet->setCellValue($char_column.'1', $columnNames[$iterator]);
            $sheet->getStyle($char_column.'1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('888888');

            $sheet->getStyle($char_column.'1')
                ->getFont()
                ->getColor()
                ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

            $sheet->getStyle($char_column.'1')
                ->getFont()
                ->setSize(11);

            $sheet->getStyle($char_column.'1')
                ->getFont()
                ->setBold(true);
            $sheet->getColumnDimension($char_column)->setAutoSize(true);

            $current_column++;
        }

        $currentRow = 2;

        foreach($cycles as $cycle){
            $rows = DB::table('order_pages')
                ->select(
                    'd_cycles.id as cycleId',
                    'users.id as userId',
                    'username',
                    'page',
                    DB::raw('
                        (SUM(order_page_columns.total_amount_2d) + 
                        SUM(order_page_columns.total_amount_3d)) as "total_sale"'),
                    DB::raw('SUM(order_page_columns.total_win_2d) as "win_2d"'),
                    DB::raw('SUM(order_page_columns.total_win_3d) as "win_3d"')
                )
                ->join('order_page_columns','order_pages.id','=','order_page_columns.order_page_id')
                ->join('d_cycles','order_pages.cycle_id','=','d_cycles.id')
                ->join('users','users.id','=','order_pages.user_id')
                ->where('order_pages.cycle_id',$cycle->id)
                ->groupBy('d_cycles.id','users.id','username','page')
                ->get();

                // $query = str_replace(array('?'), array('\'%s\''), $row->toSql());
                // $query = vsprintf($query, $row->getBindings());

                // return $query;

                foreach($rows as $row){
                    $sheet->setCellValue('A'.$currentRow, date('Y-m-d', $cycle->result_time));
                    $sheet->setCellValue('B'.$currentRow, date('H:i A', $cycle->result_time));
                    $sheet->setCellValue('C'.$currentRow, $row->username);
                    $sheet->setCellValue('D'.$currentRow, $row->page);
        
                    $sheet->setCellValue('E'.$currentRow, $row->total_sale);
                    $sheet->setCellValue('F'.$currentRow, $row->win_2d);
                    $sheet->setCellValue('G'.$currentRow, $row->win_3d);
        
                    $sheet->setCellValue('H'.$currentRow, $row->win_2d + $row->win_3d);
                    $currentRow++;
                }
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save('data.xlsx');
        
        return response()->download('data.xlsx');
    }

    function downloadReport(){
        $beginDate = request()->get('begin_date');
        $endDate = request()->get('end_date');
        $account_id = request()->get('account_id');

        if($account_id){
            $user = User::whereAccountNumber($account_id)->first();
        }

        $data = DB::table('order_pages')
                ->select(
                    DB::raw('Date(order_pages.created_at) as date'),
                    DB::raw('SUM(order_pages.total_amount_2d) as "total_2d"'),
                    DB::raw('SUM(order_pages.total_amount_3d) as "total_3d"'),
                    DB::raw('SUM(order_pages.total_amount_5d) as "total_5d"'),
                    DB::raw('SUM(order_page_columns.total_win_2d) as "win_2d"'),
                    DB::raw('SUM(order_page_columns.total_win_3d) as "win_3d"'),
                    DB::raw('SUM(order_page_columns.total_win_5d) as "win_5d"')
                )
                ->join('order_page_columns','order_pages.id','=','order_page_columns.order_page_id')
                ->whereBetween('order_pages.created_at',[$beginDate,$endDate])
                ->groupBy('date');
                
        if(isset($user)){
            $data = $data->where('order_pages.user_id', $user->id);
        }

        $data = $data->get();

        $spreadsheet = new Spreadsheet();
        $columnNames = [
            'Date', 'Amount 2D', 'Amount 3D', 'Amount 5D',' Win 2D','Win 3D','Win 5D','Total','Total Win'
        ];

        $excelSheet = $spreadsheet->getActiveSheet();
        $current_column = 65;

        //render column
        for($iterator=0;$iterator < count($columnNames);$iterator++){
            $char_column = chr($current_column);
            $excelSheet->setCellValue($char_column.'1', $columnNames[$iterator]);
            $excelSheet->getStyle($char_column.'1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('888888');

            $excelSheet->getStyle($char_column.'1')
                ->getFont()
                ->getColor()
                ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

            $excelSheet->getStyle($char_column.'1')
                ->getFont()
                ->setSize(11);

            $excelSheet->getStyle($char_column.'1')
                ->getFont()
                ->setBold(true);
            $excelSheet->getColumnDimension($char_column)->setAutoSize(true);

            $current_column++;
        }
        $currentRow = 2;
        $sumTotal = 0;
        $sumWin = 0;
        foreach($data as $row){
            
            $total = $row->total_2d + $row->total_3d + $row->total_5d;
            $totalWin = $row->win_2d + $row->win_3d + $row->win_5d;

            $sumTotal += $total;
            $sumWin += $totalWin;

            $excelSheet->setCellValue('A'.$currentRow, $row->date);
            $excelSheet->setCellValue('B'.$currentRow, $row->total_2d);
            $excelSheet->setCellValue('C'.$currentRow, $row->total_3d);
            $excelSheet->setCellValue('D'.$currentRow, $row->total_5d);

            $excelSheet->setCellValue('E'.$currentRow, $row->win_2d);
            $excelSheet->setCellValue('F'.$currentRow, $row->win_3d);
            $excelSheet->setCellValue('G'.$currentRow, $row->win_5d);

            $excelSheet->setCellValue('H'.$currentRow, $total);
            $excelSheet->setCellValue('I'.$currentRow, $totalWin);
            
            $currentRow++;
        }

        $excelSheet->setCellValue('G'.$currentRow, 'Total: ');
    
        $excelSheet->setCellValue('H'.$currentRow, $sumTotal);
        $excelSheet->setCellValue('I'.$currentRow, $sumWin);

        $writer = new Xlsx($spreadsheet);
        $writer->save('data.xlsx');
        
        return response()->download('data.xlsx');
    }
}
