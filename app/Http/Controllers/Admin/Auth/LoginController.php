<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;    
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Session;
use Hash;
use App\User;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/dashboard';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(){
        return view('admin.auth.login-form');
    }

    public function login(Request $request){
	    
	$this->validateLogin($request);
        
        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    public function attemptLogin(Request $request)
    {
        $user = User::where(['username' => $request->username])->first();
        if(!$user){
            $user = User::where(['account_number' => $request->username])->first();
        }
        if(!$user || $user->password != md5($request->password.$user->pass_salt) 
            || $user->state !=1){
            throw ValidationException::withMessages([
                'failed' => [trans('auth.failed')],
            ]);
        }
        if($user) Session::put('auth',$user);
        return $user ? Auth::login($user,$request->filled('remember')) : false;
    }

    public function validateLogin(Request $request)
    {   
        $request->validate([
            'username' => 'required|string',
            'password' => 'required|string',
        ]);
    }

    public function credentials(Request $request)
    {
        return $request->only('email', 'password');
    }

    public function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            'failed' => [trans('auth.failed')],
        ]);
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();
        session()->forget('auth');

        return redirect('/login');
    }
}
