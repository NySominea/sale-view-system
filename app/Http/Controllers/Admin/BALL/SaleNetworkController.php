<?php

namespace App\Http\Controllers\Admin\BALL;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Constants\UserBallAccount;
use App\User;

class SaleNetworkController extends Controller
{
    public function show(){
        if(session()->get('auth')->level-1 > request()->level) abort(403);
        $level = request()->get('level') ? request()->get('level') + 1 : 1;
        $level = $level > 8 ? 8 : $level;
        $parent_id = request()->get('parent_id') ?? 0;
        $state = request()->get('state') ?? 'all';
        $keyword = request()->get('keyword') ?? '';
        
        $users = User::select('id','username','account_number','parent_id','level','com_direct','user_type','state','win_money_ball','commission_ball','current_commission_ball')
                        ->with('ballCashAccount','children')->where(['com_direct' => 0])
                    ->when($state != 'all',function($q) use ($state){
                        return $q->where(['state' => $state]);
                    })
                    ->when(request()->id,function($q){
                        return $q->where(['id' => request()->id]);
                    })
                    ->when($keyword == '',function($q) use ($level,$parent_id){
                            return $q->where(['user_type' => 2, 'level' => $level, 'parent_id' => $parent_id]);
                    })->when($keyword != '',function($q) use ($keyword){
                        return $q->where('username','LIKE','%'.$keyword.'%')
                                 ->orWhere('account_number','LIKE','%'.$keyword.'%');
                    })
                    ->orderBy('id','DESC')->paginate(20);
       
        // Breadcrumbs
        $arrayId = [];
        $topLevelUsers = [];
        $thisUser = User::select('id','username','level','l1_id','l2_id','l3_id','l4_id','l5_id','l6_id','l7_id','l8_id')
                        ->where('id',$parent_id)->first(); 

        if($thisUser){
            for($i = 1; $i < $thisUser->level; $i++){  
                $arrayId[] = $thisUser->{'l'.$i.'_id'};
            }
           
            $topLevelUsers = User::select('id','username','level')->whereIn('id', $arrayId)->orderBy('level','ASC')->get();
            $topLevelUsers[] = $thisUser;
         
        }
       
        return view('admin.BALL.user.sale_network',compact('users','topLevelUsers'));
    }
}
