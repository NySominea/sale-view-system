<?php

namespace App\Http\Controllers\Admin\BALL;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\User;
class AccountController extends Controller
{
    public function show($id){

        DB::beginTransaction();
        try{
        
            $user = User::with('userAccountBall')->whereId($id)->first();
            $accounts = $user->userAccountBall;
            if(!$user)
                return redirect()->back()->withInput()->withError('Unknown User!');
            DB::commit();
            
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withInput()->withError('There was an error during operation!');
        }
        return view('admin.BALL.user.account',compact('user','accounts'));
    }
}
