<?php

namespace App\Http\Controllers\Admin\BALL;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\BALL\Cycle;
use App\Http\Controllers\Admin\BALL\OrderTempController;
use App\Model\BALL\Order;

class OrderController extends Controller
{
    public function showSelectionForm(){

        $cycles = Cycle::orderBy('cycle_sn','DESC')->where(['state' => 0, 'has_released' => 1])->pluck('cycle_sn','id')->toArray();
        $cycles = ['0' => 'Choose'] + $cycles;
    
        return view('admin.BALL.order.report.selection-form',compact('cycles'));
    }
    
    public function show(Request $request){

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');

        $showTicketDetail = false;
        $data = [];
        $breadcrumbs = "";
        $orderTempController = new OrderTempController;
        $cycle = $request->cycle_id ? Cycle::findOrFail($request->cycle_id) : null;
        $table = Order::getModel()->getTable();
        if(!$request->user_id){
            $d = $orderTempController->structureSummaryData($table, $request);
            $total = isset($d['total']) ? $d['total'] : null; 
            $users = isset($d['data']) && count($d['data']) > 0 ? $d['data'] : null;
            $hasDirectUser = $d['hasDirectUser'];
            $parentHasOrder = $d['parentHasOrder'];
        }else{
            $d = $orderTempController->structureTicketDataForEachUser($table, $request);
            $total = isset($d['total']) ? $d['total'] : null;
            $users = isset($d['data'][0]) ? $d['data'][0] : null;
            $showTicketDetail = true;
            $hasDirectUser = $d['hasDirectUser'];
            $parentHasOrder = $d['parentHasOrder'];
        }
        
        // Breadcrumbs
        $topLevelUsers = $orderTempController->getBreadcrumbList($request);
      
        return view('admin.BALL.order.report.index',compact('cycle','users','total','topLevelUsers','showTicketDetail','hasDirectUser','parentHasOrder'));
    }

    public function ajaxGetOrderDetail($ticket){

        $success = false;
        $data = [];
        $orders = Order::with('user')->whereTicket($ticket)->get();
        if($orders && $orders->count() > 0){
            $data = [
                'ticket' => $orders[0]->ticket,
                'betAmount' => currencyFormat($orders->sum('amount')),
                'winAmount' => currencyFormat($orders->sum('win_amount')),
                'created_at'=> date('Y-m-d H:i A', strtotime($orders[0]->created_at)),
            ];

            if($orders[0]->user->level != 0){
                for($i=1;$i<$orders[0]->user->level + 1;$i++){ 
                    $data['rebates'][] = [
                        'levelTitle' => getUserLevelTitle($i),
                        'username' => $orders[0]->parent($i)->username,
                        'rebate' => currencyFormat($orders->sum('l'.$i.'_rebate'))
                    ];
                }
            }else{
                $data['rebates'][] = [
                    'levelTitle' => "Direct User",
                    'username' => $orders[0]->user->username,
                    'rebate' => currencyFormat(0)
                ];
            }
            $success = true;
        }
        return response()->json(['success' => $success, 'data' => $data]);
    }

    public function downloadExcel(){
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');

        $orderTempController = new OrderTempController;
        $table = Order::getModel()->getTable();  
        $request = request();
        $data = $orderTempController->structureTicketDataForEachUser($table,$request);
        $path = $orderTempController->writeExcel($data,true);
        return response()->json(['path' => $path, 'success' => true]);
    }
}
