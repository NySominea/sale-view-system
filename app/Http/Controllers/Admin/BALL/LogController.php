<?php

namespace App\Http\Controllers\Admin\BALL;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\BALL\UserAccountLog;
use App\User;
use App\Constants\Account;

class LogController extends Controller
{
    public function show($id){
    
        $log_type = request()->log_type ?? 'all';
        $begin_time = request()->begin_time ?? '';
        $end_time = request()->end_time ?? '';
        
        $user = User::findOrFail($id);
        $logs = UserAccountLog::with('account','toUser')->where('user_id',$id);
        if($log_type != 'all'){
            $logs->where('log_type',$log_type);
        }
        if($begin_time && $end_time){
            $logs->whereBetween('created_at',[strtotime($begin_time),strtotime($end_time)]);
        }
       
        $logs = $logs->orderBy('created_at','DESC')->paginate(20);
        return view('admin.BALL.user.log',compact('user','logs'));
    }
    public function ajaxGetLogInformation($id){
       
        $data = [];
        $result = false;
        $log = UserAccountLog::with('user','manager','toUser')->whereId($id)->first();
        if($log){
            $result = true;
            $data = [
                'transaction' => $log->log_number,
                'username' => $log->user ? $log->user->username : '',
                'manager' => $log->manager ? $log->manager->username : '',		
                'logType' => $log->log_type == Account::LOG_TYPE_IN ? 'In' : 'Out',	
                'amount' => currencyFormat($log->amount),	
                'balance' => currencyFormat($log->balance),

                'toAccount' => $log->to_type == Account::SYSTEM_ACCOUNT_TYPE 
                            ? getAdminLanguageData()[Account::systemAccountType($log->to_type)]
                            : getAdminLanguageData()[Account::systemAccountType($log->to_type)].($log->toUser ? '('.$log->toUser->username.')' : ''),	
                'dateTime' => (string) $log->created_at,
                'abstract' => getAdminLanguageData()[$log->abstract],
                'auditor' => '',
                'customSignature' => ''
            ];
        }
        return response()->json(['success' => $result, 'log' => $data]);
    }
  
}
